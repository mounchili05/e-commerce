<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipping_infos', function (Blueprint $table) {
            $table->string('name')->nullable()->after('id');
            $table->decimal('price', 20, 2)->nullable()->after('postal_code');
            $table->string('product_name')->nullable()->after('postal_code');
            $table->integer('quantity')->nullable()->after('postal_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_infos', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('price');
            $table->dropColumn('product_name');
            $table->dropColumn('quantity');
        });
    }
};
