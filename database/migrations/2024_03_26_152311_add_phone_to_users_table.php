<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone2')->nullable()->after('email');
            $table->string('phone1')->nullable()->after('email');
            $table->timestamp('date_of_birth')->nullable()->after('user_type');
            $table->string('place_of_residence')->nullable()->after('user_type');
            $table->string('id_card_number')->nullable()->after('user_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone2');
            $table->dropColumn('phone1');
            $table->dropColumn('date_of_birth');
            $table->dropColumn('place_of_residence');
            $table->dropColumn('id_card_number');
        });
    }
};
