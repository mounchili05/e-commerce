<!DOCTYPE html>
<html lang="en" xml:lang="en">
  <head>
    <title></title>
    <meta content="text/html; charset=utf-8" />
  </head>

  <body>
    <div class="" style="font-weight: 500; margin-bottom: 0;">
      <h4>Réinitialiser votre mot de passe</h4>

      @php($lastToken = $user->lastPasswordResetToken())
      @php($link = route('reset-password', ['token' => $lastToken->{'token'}]))

      Vous pouvez réinitialiser votre mot de passe en utilisant lien ci-dessous :
      <a href="{{ $link }}" style="display: block; margin-bottom: 1rem;">{{ $link }}</a>
    </div>

    @include('emails.mail-footer')
  </body>
</html>
