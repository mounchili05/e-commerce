@extends('admin.layouts.template')
@section('page_title')
Modifier image article - E-commerce
@endsection
@section('content')
<!-- Basic layout -->
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Admin/</span> Modifier image articles</h4>
    <div class="col-xxl">
        <div class="card mb-4">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h5 class="mb-0">Modifier cette image</h5>
            <small class="text-muted float-end">Information sur les champs</small>
          </div>
          <div class="card-body">
            <form action="{{ route('updateproductimg')}}" method="POST" enctype="multipart/form-data">
                @csrf
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Image précédente</label>
                <div class="col-sm-10">
                  <img style="height: 200px;" src="{{ asset($productInfo->{'product_img'}) }}" alt="">
                </div>
              </div>
              <input type="hidden" value="{{ $productInfo->{'id'} }}" name="id" />

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Téléverser nouvelle image</label>
                <div class="col-sm-10">
                  <input type="file" class="form-control" id="product_img" name="product_img" />
                </div>
              </div>

              <div class="row justify-content-end">
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Modifier image</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
</div>
@endsection
