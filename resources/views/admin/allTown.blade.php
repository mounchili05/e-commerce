@extends('admin.layouts.template')
@section('page_title')
Liste ville - E-commerce
@endsection
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Admin/</span> Liste Villes</h4>
    <!-- Bootstrapp Table whith header - Ligth -->

    @if (session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
    <div class="card">
        <h5 class="card-header">Villes applicables</h5>
        <div class="card-datatable table-responsive text-nowrap">
          <table id="town-table" class="datatables table">
            <thead class="table-light">
              <tr>
                <th>Nom ville</th>
                <th>Code</th>
                <th>Distric / Quartier</th>
                <th>Frais de livraison</th>
                <th>pays</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody class="table-border-bottom-0">
                @foreach ($towns as $town)

              <tr>
                <td>{{ $town->{'town_name'} }}</td>
                <td>{{ $town->{'code'} }}</td>
                <td>{{ $town->{'district'} }}</td>
                <td>{{ getPrice($town->{'shipping_cost'}) }}</td>
                <td>{{ $town->{'country_name'} }}</td>
                <td>
                    <a href="{{ route('edittown', $town->{'id'}) }}" class="btn btn-primary">Modifier</a>
                    <a href="{{ route('deletetown', $town->{'id'}) }}" class="btn btn-warning">Supprimer</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
</div>
@endsection
@section('page-script')

    <script type="text/javascript">

        $(document).ready(function (){
            datatableInstance = $("#town-table").DataTable({
                "processing": true,
                "language": {
                "search": '',
                "searchPlaceholder": "Rechercher..."
                },
            });
        });
    </script>
@endsection
