@extends('admin.layouts.template')
@section('page_title')
Modifier article - E-commerce
@endsection
@section('content')
<!-- Basic layout -->
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Admin/</span> Modifier articles</h4>
    <div class="col-xxl">
        <div class="card mb-4">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h5 class="mb-0">Modifier article</h5>
            <small class="text-muted float-end">Information sur les champs</small>
          </div>
          <div class="card-body">
            <form action="{{ route('updateproduct')}}" method="POST">
                @csrf
              <input type="hidden" name="id" value="{{ $productInfo->{'id'} }}">
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Nom article</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Saisissez le nom de l'article" value="{{ $productInfo->{'product_name'} }}" />
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Code article</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="code" name="code" placeholder="Saisissez le code de l'article" value="{{ $productInfo->{'code'} }}" />
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Prix article</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" id="price" name="price" placeholder="Saisissez le prix de l'article" value="{{ $productInfo->{'price'} }}" />
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Quantité article</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Saisissez le prix de l'article" value="{{ $productInfo->{'quantity'} }}" />
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Petite description article</label>
                <div class="col-sm-10">
                    <input class="form-control" name="product_short_desc" type="text" value="{{ $productInfo->{'product_short_desc'} }}">
                  <!-- <textarea class="form-control" name="product_short_desc" id="product_short_desc" cols="20" rows="5">{{ $productInfo->{'product_short_desc'} }}</textarea>-->
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Description article</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="product_desc" id="product_desc" cols="30" rows="10">{{ $productInfo->{'product_desc'} }}</textarea>
                </div>
              </div>

              <div class="row justify-content-end">
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Modifier article</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
</div>
@endsection
