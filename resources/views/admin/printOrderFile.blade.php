<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bon de commande</title>

    <div  style="margin-bottom: 20px; height: 250px;">
        <div style="width: 50%; float: left;" class="col-lg-4 text-left">
            @if (env('APP_ENV') == 'local')
            <img style="width: 50%" src="data:image/png;base64,{{ base64_encode(file_get_contents(public_path('assets/img/brand/ecom4.jpg'))) }}">
          @else
            <img style="width: 50%" src="data:image/png;base64,{{ base64_encode(file_get_contents(public_path('assets/img/brand/ecom4.jpg'))) }}">
          @endif

          <h4 class="mt-0">CHOCHO MARKET</h4>
            <span> Yaoundé - Obili </span><br />
            <span style="margin-bottom: 5px"> TEL : (+237) 674816238 / 696470157 </span><br /><br />


            <span><i> Vous satisfaire est notre priorité</i> </span><br /><br />
        </div>
        <div style="width: 25%; float: right; margin-top: 150px;">
            <span style=""><strong>A </strong>{{ $orderName }}</span><br />
            <span style="">{{ $orderLocality }}<br />
            <span class="mt-0"><strong>TEL: </strong>{{ $orderPhone }}</span>
            <h5 class="mt-0"><strong>Commande N<sup>0</sup>: </strong>{{ $orderId }} </h5>
        </div>
    </div>

</head>
<body style="font-family: Arial, sans-serif; display:flex;">

    <h2 style="text-align: center;"><u>Bon de commande</u></h2>
    <table style="border-collapse: collapse; width: 100%;">
        <tr style="background-color: #abc7db;">
            <th style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Date commande</th>
            {{--  <th style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Receveur</th>  --}}
            <th style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Canal commande</th>
            <th style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Livraison</th>
            <th style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Point de livraison</th>
            <th style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Modalités</th>
        </tr>
        <tr>
            <td style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">{{ $orderCreated }}</td>
            {{--  <td style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">{{ $orderName }}</td>  --}}
            <td style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Site web CHOCHO MARKET</td>
            <td style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Le {{date('d/m/Y')}} par un transporteur privé</td>
            <td style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">{{ $orderLocality }}</td>
            <td style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Paiement à la livraison</td>
        </tr>
    </table>

    <br />
    <br />

    <table style="border-collapse: collapse; width: 100%;">
        <tr style="border: 1px solid; background-color: #abc7db;">
            <th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Nom article</th>
            <th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Quantité</th>
            <th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Montant</th>
        </tr>
        @php
            $total = 0
        @endphp
        @foreach ($orders as $order)
        @php
            $total = $total + $order->price;
        @endphp
        <tr style="border: 1px solid">
            <td style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">{{ $order->product_name }}</td>
            <td style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">{{ $order->quantity }}</td>
            <td style=" padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">{{ getPrice($order->price) }}</td>
        </tr>
        @endforeach
        <tr style="border: 1px solid">
            <td colspan="2" style="border: 1px solid; text-align:right; padding: 8px;">
                <strong>Frais de livraison</strong>
            </td>
            <td style="border: 1px solid; padding: 8px;">
                <span >{{ getPrice($shippingCost) }}</span>
            </td>
        </tr>
        <tr style="border: 1px solid">
            <td colspan="2" style="border: 1px solid; text-align:right; padding: 8px;">
                <strong>TOTAL</strong>
            </td>
            <td style="border: 1px solid; padding: 8px;">
                <span style="color: red"><strong>{{getPrice($total + $shippingCost)}}</strong></span>
            </td>
        </tr>
    </table>

    <footer style="background: #f1f1f1; position:absolute; bottom: 0; left: 0; width: 100%;">
        <div><hr/></div>
        <div style="text-align: center; color:cadetblue">
            <span >&copy; {{date('Y')}}</span> <span>Merci de votre confiance</span>

        </div>
    </footer>
</body>
</html>
