@extends('admin.layouts.template')
@section('page_title')
Commandes en attente - E-commerce
@endsection
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Admin/</span> Commandes en attente</h4>
    @if (session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif

    <div class="card mb-4">
        <div class="card-widget-separator-wrapper">
          <div class="card-body card-widget-separator">
            <div class="row gy-4 gy-sm-1">
              <div class="col-sm-6 col-lg-3">
                <div class="d-flex justify-content-between align-items-start card-widget-1 border-end pb-3 pb-sm-0">
                  <div>
                    <h6 class="mb-2">Quantité articles par catégorie</h6>
                    @foreach ($categories as $category)
                    <p class="mb-0"><span class="text-muted me-2">{{$category->category_name}}</span><span class="badge bg-label-success">{{$category->product_count}}</span></p>
                    @endforeach
                  </div>
                  <span class="avatar me-sm-4">
                    <span class="avatar-initial bg-label-secondary rounded"><i class="bx bx-category text-body"></i></span>
                  </span>
                </div>
                <hr class="d-none d-sm-block d-lg-none me-4">
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="d-flex justify-content-between align-items-start card-widget-2 border-end pb-3 pb-sm-0">
                  <div>
                    <h6 class="mb-2">Quantité articles par sous-catégorie</h6>
                    @foreach ($subCategories as $subCategory)
                    <p class="mb-0"><span class="text-muted me-2">{{$subCategory->subcategory_name}}</span><span class="badge bg-label-success">{{$subCategory->product_count}}</span></p>
                    @endforeach
                  </div>
                  <span class="avatar p-2 me-lg-4">
                    <span class="avatar-initial bg-label-secondary rounded"><i class="bx bx-buildings text-body"></i></span>
                  </span>
                </div>
                <hr class="d-none d-sm-block d-lg-none">
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="d-flex justify-content-between align-items-start border-end pb-3 pb-sm-0 card-widget-3">
                  <div>
                    <h6 class="mb-2">Articles vendus</h6>
                    @foreach ($products as $product)
                    <p class="mb-0"><span class="text-muted me-2">{{$product->product_name}}</span><span class="badge bg-label-success">{{getPrice($product->total_price_seller) }}</span></p>
                    @endforeach
                  </div>
                  <span class="avatar p-2 me-sm-4">
                    <span class="avatar-initial bg-label-secondary rounded"><i class="bx bx-bitcoin text-body"></i></span>
                  </span>
                </div>
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="d-flex justify-content-between align-items-start">
                  <div>
                    <h6 class="mb-2">Quantité articles vendus</h6>
                    @foreach ($products as $product)
                    <p class="mb-0"><span class="text-muted me-2">{{$product->product_name}}</span><span class="badge bg-label-success">{{$product->quantity_seller_count }}</span></p>
                    @endforeach
                  </div>
                  <span class="avatar p-2">
                    <span class="avatar-initial bg-label-secondary rounded"><i class="bx bx-diamond text-body"></i></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    <div class="card">
        <h5 class="card-header">Liste</h5>
        <div class="card-datatable table-responsive text-nowrap">
            <table id="pending-table" class="datatables table table-bordered">
                <thead>
                    <tr>
                        <th>Utilisateur</th>
                        <th>Informations de livraison</th>
                        <th>Nom de larticle</th>
                        <th>Quantité</th>
                        <th>Total à payer</th>
                        <th>Statut</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                    @foreach ($pendingOrders as $order)
                        <tr>
                            <td>{{ $order->{'name'} }}</td>
                            <td>
                                <ul>
                                    <li>Numéro téléphone : <b><i style="color:darkgreen">{{ $order->{'phone_number'} }}</i></b></li>
                                    <li>Ville/quartier : <b><i style="color:darkgreen">{{ $order->{'city_name'} }}</i></b></li>
                                    <li>Boîte postale : <b><i style="color:darkgreen">{{ $order->{'postal_code'} }}</i></b></li>
                                </ul>
                            </td>
                            <td>{{ $order->{'product_name'} }}</td>
                            <td>{{ $order->{'quantity'} }}</td>
                            <td>{{ getPrice($order->{'price'}) }}</td>
                            <td>
                                @if ($order->{'status'} == "En attente")
                                    <span class="badge bg-warning">{{ $order->{'status'} }}</span>
                                @else
                                    <span class="badge bg-success">{{ $order->{'status'} }}</span>
                                @endif
                            </td>
                            <td>
                                @if ($order->{'status'} == "En attente")
                                <a href="{{route('confirmorder', [$order->{'id'}, $order->{'quantity'}, $order->{'product_name'}, $order->{'price'}] )}}" class="btn btn-primary">Confirmer</a>
                                @else
                                    <span>/</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('page-script')

    <script type="text/javascript">

        $(document).ready(function (){

            datatableInstance = $("#pending-table").DataTable({
                "processing": true,
                "language": {
                "search": '',
                "searchPlaceholder": "Rechercher..."
                },
            });
        });
    </script>
@endsection
