@extends('admin.layouts.template')
@section('page_title')
Liste sous-catégorie - E-commerce
@endsection
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Admin/</span> Liste sous-catégories</h4>
    <!-- Bootstrapp Table whith header - Ligth -->

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="card">
        <h5 class="card-header">Sous-catégories applicables</h5>
        <div class="card-datatable table-responsive text-nowrap">
          <table id="subcategory-table" class="datatables table">
            <thead class="table-light">
              <tr>
                <th>N<sup>o</sup></th>
                <th>Nom Sous-catégorie</th>
                <th>Nom Catégorie</th>
                <th>Quantité article</th>
                <th>slug</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody class="table-border-bottom-0">
                @foreach ($subCategories as $subCategory)
                    <tr>
                    <td>{{ $subCategory->{'id'} }}</td>
                    <td>{{ $subCategory->{'subcategory_name'} }}</td>
                    <td>{{ $subCategory->{'category'} != null ? $subCategory->{'category'}->{'category_name'} : '/' }}</td>
                    <td>{{ $subCategory->{'product_count'} }}</td>
                    <td>{{ $subCategory->{'slug'} }}</td>
                    <td>
                        <a href="{{ route('editsubcategory', $subCategory->{'id'}) }}" class="btn btn-primary">Modifier</a>
                        <a href="{{ route('deletesubcategory', $subCategory->{'id'}) }}" class="btn btn-warning">Supprimer</a>
                    </td>
                    </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
</div>
@endsection
@section('page-script')

    <script type="text/javascript">

        $(document).ready(function (){
            datatableInstance = $("#subcategory-table").DataTable({
                "processing": true,
                "language": {
                "search": '',
                "searchPlaceholder": "Rechercher..."
                },
            });
        });
    </script>
@endsection
