@extends('admin.layouts.template')
@section('page_title')
Compte admin - E-commerce
@endsection
@section('content')

<!-- Add User modal -->
<div class="modal fade" id="addUserModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajouter compte</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="addUserForm">
                    @csrf
                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="addname">Nom complet</label>
                        <input type="text" class="form-control" id="addname" name="name" placeholder="Saisissez le nom complet" />
                    </div>
                  </div>
                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="addphone1">N<sup>o</sup> téléphone 1 </label>
                        <input type="text" class="form-control" id="addphone1" name="phone1" placeholder="Saisissez le numéro de téléphone 1" />
                    </div>
                  </div>
                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="addphone2">N<sup>o</sup> téléphone 2 </label>
                        <input type="text" class="form-control" id="addphone2" name="phone2" placeholder="Saisissez le numéro de téléphone 2" />
                    </div>
                  </div>

                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="addemail">Email </label>
                        <input type="text" class="form-control" id="addemail" name="email" placeholder="Saisissez lemail" />
                    </div>
                  </div>

                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="adddate_of_birth">Date de naissance </label>
                        <input type="date" class="form-control" id="adddate_of_birth" name="date_of_birth" placeholder="Saisissez la date de naissance" />
                    </div>
                  </div>

                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="addplace_of_residence">Lieu de résidence </label>
                        <input type="text" class="form-control" id="addplace_of_residence" name="place_of_residence" placeholder="Saisissez le lieu de résidence" />
                    </div>
                  </div>

                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="addid_card_number">N<sup>o</sup> de la pièce didentité </label>
                        <input type="text" class="form-control" id="addid_card_number" name="id_card_number" placeholder="Saisissez le numéro de la pièce d'identité" />
                    </div>
                  </div>

                    <div class="row form-group">
                        <div class="col mb-3">
                            <label for="password" class="form-label">Mot de passe</label>
                            <div class="input-group input-group-merge">
                                <input id="addpassword" name="password" type="password" class="form-control">
                                <span class="input-group-text"><i class="bx bx-lock"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col mb-3">
                            <label for="confirmPassword" class="form-label">Confirmer mot de passe</label>
                            <div class="input-group input-group-merge">
                                <input id="addpassword_confirmation" name="password_confirmation" type="password" class="form-control">
                                <span class="input-group-text"><i class="bx bx-lock"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col mb-3">
                            <label class="form-label"> Sélectionner un rôle</label>
                            <select class="form-select form-control" id="addrole_id" name="role_id" aria-label="Default select example">
                                <option selected>Sélectionnez un rôle</option>
                                @foreach ($roles as $role)
                                <option value="{{ $role->{'id'} }}">{{ $role->{'name'} }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                    data-bs-dismiss="modal">Fermer</button>
                <button id="addUserSaveButton" type="button"
                    class="btn btn-primary">Enregistrer</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit User modal -->
<div class="modal fade" id="editUserModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modifier compte</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="editUserForm">
                    @csrf
                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="name">Nom complet</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Saisissez le nom complet" />
                    </div>
                  </div>
                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="phone1">N<sup>o</sup> téléphone 1 </label>
                        <input type="text" class="form-control" id="phone1" name="phone1" placeholder="Saisissez le numéro de téléphone 1" />
                    </div>
                  </div>
                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="phone2">N<sup>o</sup> téléphone 2 </label>
                        <input type="text" class="form-control" id="phone2" name="phone2" placeholder="Saisissez le numéro de téléphone 2" />
                    </div>
                  </div>

                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="email">Email </label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="Saisissez lemail" />
                    </div>
                  </div>

                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="date_of_birth">Date de naissance </label>
                        <input type="date" class="form-control" id="date_of_birth" name="date_of_birth" placeholder="Saisissez la date de naissance" />
                    </div>
                  </div>

                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="place_of_residence">Lieu de résidence </label>
                        <input type="text" class="form-control" id="place_of_residence" name="place_of_residence" placeholder="Saisissez le lieu de résidence" />
                    </div>
                  </div>

                  <div class="row">
                    <div class="col mb-3">
                        <label class="form-label" for="id_card_number">N<sup>o</sup> de la pièce didentité </label>
                        <input type="text" class="form-control" id="id_card_number" name="id_card_number" placeholder="Saisissez le numéro de la pièce d'identité" />
                    </div>
                  </div>

                  <div class="row form-group">
                    <div class="col mb-3">
                        <label class="form-label"> Sélectionner un rôle</label>
                        <select class="form-select form-control" id="role_id" name="role_id" aria-label="Default select example">
                            <option selected>Sélectionnez un rôle</option>
                            @foreach ($roles as $role)
                            <option value="{{ $role->{'id'} }}">{{ $role->{'name'} }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                    data-bs-dismiss="modal">Fermer</button>
                <button id="editUserSaveButton" type="button"
                    class="btn btn-primary">Enregistrer</button>
            </div>
        </div>
    </div>
</div>

<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Admin/</span> Comptes administrateurs</h4>
    <!-- Bootstrapp Table whith header - Ligth -->

    @if (session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
    <div class="card">
        <h5 class="card-header">Comptes actifs</h5>
        <div class="card-datatable table-responsive text-nowrap">
          <table id="user-table" class="datatables table table-bordered">
            <thead class="table-light">
              <tr>
                <th>N<sup>o</sup></th>
                <th>Nom complet</th>
                <th>N<sup>o</sup> téléphone 1</th>
                <th>N<sup>o</sup> téléphone 2</th>
                <th>Email</th>
                <th>Date de naissance</th>
                <th>Lieu de résidence</th>
                <th>N<sup>o</sup> de la pièce didentité</th>
                <th>Rôle</th>
                <th style="display:none">Role id</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody class="table-border-bottom-0">
                @foreach ($users as $user)
                @php
                    $roleName = $user->{'user_type'};
                    $role = App\Models\Role::query()->where('name', $roleName)->first();
                    $roleId = $role->{'id'};
                @endphp
              <tr>
                <td>{{ $user->{'id'} }}</td>
                <td>{{ $user->{'name'} }}</td>
                <td>{{ $user->{'phone1'} }}</td>
                <td>{{ $user->{'phone2'} }}</td>
                <td>{{ $user->{'email'} }}</td>
                <td>{{ $user->{'date_of_birth'} != null ? date_format($user->{'date_of_birth'}, "d/m/Y") : '/' }}</td>
                <td>{{ $user->{'place_of_residence'} }}</td>
                <td>{{ $user->{'id_card_number'} }}</td>
                <td><span class="badge bg-success">{{ $user->{'user_type'} }}</span></td>
                <td style="display:none">{{ $roleId }}</td>
                <td>
                    <a class="edit-user" about="{{ $user->{'id'} }}" href="javascript:void(0);" title="Modifier compte"><i class="bx bx-edit-alt me-1 text-primary"></i></a>
                    <a class="delete-user" about="{{ $user->{'id'} }}" href="javascript:void(0);" title="Supprimer compte"><i class="bx bx-trash me-1 text-danger"></i></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
</div>
@endsection
@section('page-script')

<script type="text/javascript">

    let userEditionId;

    $(document).ready(function (){
        datatableInstance = $("#user-table").DataTable({
            "processing": true,
            "language": {
            "search": '',
            "searchPlaceholder": "Rechercher..."
            },
            "dom": '<"row mx-2"' +
          '<"col-md-2"<"me-3"l>>' +
          '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
          '>t' +
          '<"row mx-2"' +
          '<"col-sm-12 col-md-6"i>' +
          '<"col-sm-12 col-md-6"p>' +
          '>',
          "buttons": [{
            "text": "<i class='bx bx-plus me-0 me-sm-2'></i><span class='d-none d-lg-inline-block'>Créer compte</span>",
            "className": 'add-compte btn btn-primary ms-3',
          },
            {
              "text": "<i class='bx bxs-file-pdf me-0 me-sm-2'></i><span class='d-none d-lg-inline-block'>Imprimer</span>",
              "className": 'print-country-type btn btn-secondary ms-3',
            }
          ]
        });

        //Click on .edit-user
      $("body").on("click", ".edit-user", function (event) {
        event.preventDefault();
        let id = $(this).attr("about");
        //let designation = $(this).parent().parent().children().first().html();
        let name = $(this).parent().parent().children(":eq(1)").html();
        let phone1 = $(this).parent().parent().children(":eq(2)").html();
        let phone2 = $(this).parent().parent().children(":eq(3)").html();
        let email = $(this).parent().parent().children(":eq(4)").html();
        let birth = $(this).parent().parent().children(":eq(5)").html();
        let locality = $(this).parent().parent().children(":eq(6)").html();
        let idCart = $(this).parent().parent().children(":eq(7)").html();
        let userType = $(this).parent().parent().children(":eq(8)").html();
        let roleId = $(this).parent().parent().children(":eq(9)").html();

        $("#name").val(name);
        $("#phone1").val(phone1);
        $("#phone2").val(phone2);
        $("#email").val(email);
        $("#date_of_birth").val(moment(birth).format("YYYY-MM-DD"));
        $("#date_of_birth").trigger('change');
        $("#place_of_residence").val(locality);
        $("#id_card_number").val(idCart);
        $("#role_id").val(roleId);
        $("#role_id").trigger('change');

        editUserModal = new bootstrap.Modal(document.getElementById("editUserModal"), {
          "backdrop": "static",
          "keyboard": false,
        });
        editUserModal.show();
        userEditionId = id;
      });

      //Click on editCatSaveButton
      $("#editUserSaveButton").on("click", function (event) {
        event.preventDefault();
        let $this = $(this);
        let parameters = {
          id: userEditionId
        };
        let name = $("#name").val();
        if (name === "") {
          toastr.warning("Veuillez renseigner le champ nom complet");
          return;
        }
        parameters["name"] = name;

        let email = $("#email").val();
        if (email === "") {
          toastr.warning("Veuillez renseigner le champ email");
          return;
        }
        parameters["email"] = email;

        let phone1 = $("#phone1").val();
        parameters["phone1"] = phone1;

        let phone2 = $("#phone2").val();
        parameters["phone2"] = phone2;

        let birth = $("#date_of_birth").val();
        parameters["date_of_birth"] = birth;

        let locality = $("#place_of_residence").val();
        parameters["place_of_residence"] = locality;

        let idCard = $("#id_card_number").val();
        parameters["id_card_number"] = idCard;

        let user_type = $("#role_id").val();
        parameters["role_id"] = user_type;

        parameters["_token"] = "{{ csrf_token() }}",
        //Loader.setLoading($this);
        $.ajax({
          url: "{{ route('useredition') }}",
          type: "post",
          dataType: "json",
          data: parameters,
          success: function (data) {

            if (data.code === 100) {
              editUserModal.hide();
              setTimeout(function(){
                //datatableInstance.ajax.reload();
                window.parent.location.reload();
              }, 2000);
              toastr.success("Compte modifiée avec succès");
            } else {
              //Error occurred
              toastr.error("Une erreur est survenue durant l'opération!");
            }
            //Loader.removeLoading($this);
          },
          error: function () {
            toastr.error("Une erreur est survenue durant l'opération!");
            //Loader.removeLoading($this);
          }
        });
      });

      //Click on .delete-cat
      $("body").on("click", ".delete-user", async function (event) {
        event.preventDefault();
        let userName = $(this).parent().parent().children(":eq(1)").html();
        let id = $(this).attr("about");
        {{--  let shouldDelete = await askConfirmation({
          title: "Suppression",
          message: `Voulez-vous réellement supprimer le compte' ${userName} ?`,
          cancelLabel: "Annuler",
          confirmLabel: "Confirmer",
        });  --}}

        let $this = $(this);
        if (confirm(`Voulez-vous réelement supprimer le compte ${userName} ?`)) {
          //Loader.setLoading($this);
          $.ajax({
            url: "{{ route('userdeletion') }}",
            data: {
              id: id,
              _token: "{{ csrf_token() }}"
            },
            type: "post",
            dataType: "json",
            success: function (data) {
              if (data.code === 100) {
                toastr.success("Compte supprimé avec succès!");
                setTimeout(function(){
                    //datatableInstance.ajax.reload();
                    window.parent.location.reload();
                  }, 2000);
              } else {
                toastr.error("Une erreur est survenue durant l'opération!");
              }
              //Loader.removeLoading($this);
            },
            error: function () {
              toastr.error("Une erreur est survenue durant l'opération!");
              //Loader.removeLoading($this);
            },
          });
        }
      });

      //Click on .add-compte button
      $("body").on("click", ".add-compte", function (event) {
        event.preventDefault();
        $("#addUserForm").trigger("reset");
        addUserModal = new bootstrap.Modal(document.getElementById("addUserModal"), {
          "backdrop": "static",
          "keyboard": false,
        });
        addUserModal.show();
      });

      //Click on addUserSaveButton
      $("#addUserSaveButton").on("click", function (event) {
        event.preventDefault();
        let $this = $(this);
        let parameters = {};
        let name = $("#addname").val();
        if (name === "") {
          toastr.warning("Veuillez renseigner le champ nom complet");
          return;
        }
        parameters["name"] = name;

        let password = $("#addpassword").val();
        if (password === "") {
          toastr.warning("Veuillez renseigner le champ mot de passe");
          return;
        }
        parameters["password"] = password;

        let confirmation = $("#addpassword_confirmation").val();
        if (confirmation === "") {
          toastr.warning("Veuillez renseigner le champ confirmation mot de passe");
          return;
        }
        parameters["password_confirmation"] = confirmation;

        let email = $("#addemail").val();
        if (email === "") {
          toastr.warning("Veuillez renseigner le champ email");
          return;
        }
        parameters["email"] = email;

        let phone1 = $("#addphone1").val();
        parameters["phone1"] = phone1;

        let phone2 = $("#addphone2").val();
        parameters["phone2"] = phone2;

        let birth = $("#adddate_of_birth").val();
        parameters["date_of_birth"] = birth;

        let locality = $("#addplace_of_residence").val();
        parameters["place_of_residence"] = locality;

        let idCard = $("#addid_card_number").val();
        parameters["id_card_number"] = idCard;

        let user_type = $("#addrole_id").val();
        parameters["role_id"] = user_type;

        parameters["_token"] = "{{ csrf_token() }}",

        //Loader.setLoading($this);
        $.ajax({
          url: "{{ route('usercreation') }}",
          type: "post",
          //dataType: "json",
          data: parameters,
          success: function (data) {
            if (data.code === 100) {
              $("#addUserForm").trigger("reset");
              setTimeout(function(){
                //datatableInstance.ajax.reload();
                window.parent.location.reload();
              }, 2000);
              toastr.success("Compte créé avec succès");
            }  else {
              //Error occurred
              toastr.error("Une erreur est survenue durant l'opération!");
            }
            //Loader.removeLoading($this);
          },
          error: function () {
            toastr.error("Une erreur est survenue durant l'opération!");
            //Loader.removeLoading($this);
          }
        });
      });

    });
</script>
@endsection
