@extends('admin.layouts.template')
@section('page_title')
Liste articles - E-commerce
@endsection
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Admin/</span> Liste articles</h4>
    <!-- Bootstrapp Table whith header - Ligth -->
    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif

    <div class="card">
        <h5 class="card-header">Articles disponibles</h5>
        <div class="card-datatable table-responsive text-nowrap">
          <table id="product-table" class="datatables table">
            <thead class="table-light">
              <tr>
                <th style="text-align: center;" >Id</th>
                <th style="text-align: center;">Nom article</th>
                <th style="text-align: center;">Code article</th>
                <th style="text-align: center;">Image</th>
                <th style="text-align: center;">Prix</th>
                <th style="text-align: center;">Quantité</th>
                <th style="text-align: center;">Petite description</th>
                <th style="text-align: center;">Longue description</th>
                <th style="text-align: center;">Catégorie</th>
                <th style="text-align: center;">Sous-catégorie</th>
                <th style="text-align: center;">Slug</th>
                <th style="text-align: center;">Actions</th>
              </tr>
            </thead>
            <tbody class="table-border-bottom-0">
                @foreach ($products as $product)
                <tr>
                    <td>{{ $product->{'id'} }}</td>
                    <td>{{ $product->{'product_name'} }}</td>
                    <td>{{ $product->{'code'} }}</td>
                    <td>
                        <img style="height: 100px;" src="{{ asset($product->{'product_img'}) }}" alt="">
                        <br />
                        <a style="margin-left: 40px; margin-top: 10px;" href="{{route('editproductimg', $product->{'id'})}}" class="btn btn-secondary">Modifier</a>
                    </td>
                    <td>{{ number_format($product->{'price'}, 2, ',', ' ') . " " . "F CFA" }}</td>
                    <td>{{ $product->{'quantity'} }}</td>
                    <td class="text-wrap w-px-400">{{ $product->{'product_short_desc'} }}</td>
                    <td class="text-wrap w-px-400" style="text-align: justify;">{{ $product->{'product_desc'} }}</td>
                    <td>{{ $product->{'category'} != null ? $product->{'category'}->{'category_name'} : '/' }}</td>
                    <td>{{ $product->{'subcategory'} != null ? $product->{'subcategory'}->{'subcategory_name'} : '/' }}</td>
                    <td>{{ $product->{'slug'} }}</td>
                    <td>
                        <a href="{{ route('editproduct', $product->{'id'}) }}" class="btn btn-primary">Modifier</a>
                        <a href="{{ route('deleteproduct', $product->{'id'}) }}" class="btn btn-warning">Supprimer</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
</div>
@endsection
@section('page-script')

    <script type="text/javascript">

        $(document).ready(function (){

            datatableInstance = $("#product-table").DataTable({
                "processing": true,
                "language": {
                "search": '',
                "searchPlaceholder": "Rechercher..."
                },
            });
        });
    </script>
@endsection
