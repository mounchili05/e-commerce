@extends('admin.layouts.template')
@section('page_title')
Modifier sous-catégorie - E-commerce
@endsection
@section('content')
<!-- Basic layout -->
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Admin/</span> Modifier sous-catégorie</h4>
    <div class="col-xxl">
        <div class="card mb-4">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h5 class="mb-0">Modifier la sous-catégorie</h5>
            <small class="text-muted float-end"></small>
          </div>
          <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error )
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>

            @endif
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif

            @if (session()->has('danger'))
                <div class="alert alert-danger">
                    {{ session()->get('danger') }}
                </div>
            @endif
            <form action="{{route('updatesubcategory')}}" method="POST">
                @csrf
                <input type="hidden" value="{{ $subCategoryInfo->{'id'} }}" name="id">
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Nom sous-catégorie</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="subcategory_name" name="subcategory_name" value="{{ $subCategoryInfo->{'subcategory_name'} }}" />
                </div>
              </div>

              <!-- <div class="row mb-3">
                <label for="category_name" class="col-sm-2 col-form-label">Nom sous-catégorie</label>
                <div class="col-sm-10">
                    <select class="form-select" id="category_id" name="category_id" aria-label="Default select example">
                        <option value="{{ $subCategoryInfo->{'category'} != null ? $subCategoryInfo->{'category'}->{'id'}: '' }}" selected>{{ $subCategoryInfo->{'category'} != null ? $subCategoryInfo->{'category'}->{'category_name'}: '' }}</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->{'id'} }}">{{ $category->{'category_name'} }}</option>
                        @endforeach
                    </select>
                </div>
              </div> -->

              <div class="row justify-content-end">
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Modifier sous-catégorie</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
</div>
@endsection
