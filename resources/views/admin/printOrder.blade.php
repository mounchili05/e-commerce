@extends('admin.layouts.template')
@section('page_title')
Imprimer commande - E-commerce
@endsection
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Admin/</span> Imprimer commande</h4>

    @if (session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="col-xxl">
            <div class="card mb-4">
              <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="mb-0">Imprimer la commande du client</h5>
                <small class="text-muted float-end"></small>
              </div>
              <div class="card-body">
                  <div class="row mb-3">
                    <label class="form-label" for="personName">Nom client</label>
                    <div class="col-lg-8">
                        <select class="form-select" id="personName" name="name">
                            <option value="">
                              -- Sélectionnez un nom --
                            </option>
                          </select>
                    </div>
                  </div>

                  <div class="row justify-content-end">
                    <div class="">

                      <button type="submit" id="printBtn" class="btn btn-secondary">Imprimer</button>
                    </div>
                  </div>
              </div>
            </div>
          </div>
    </div>
</div>
@endsection
@section('page-script')

    <script type="text/javascript">

        $(document).ready(function (){

            //Loading person name
            function listPersons() {
                $.ajax({
                    url: "{{ route('listpersons') }}",
                    type: "get",
                    dataType: "json",
                    success: function(data) {
                        if (data.code === 100) {
                          var monTableau = Object.keys(data.data).map(function(key) {
                            return [Number(key), data.data[key]];
                        });

                        monTableau.forEach((person) => {
                          $("#personName").append(`
                                <option value='${person[0]}'>
                                    ${person[1]}
                                </option>
                            `);
                          });

                        }
                    },
                });
            }
            listPersons();

            $("#printBtn").on("click", function(e){
              e.preventDefault();
              let personPhone = $("#personName").val()

              if(personPhone == ""){
                toastr.warning("Veuillez selectionner un nom !");
                return false;
              }
              $.ajax({
                url: "{{ route('printorderfile') }}",
                type: "get",
                dataType: "json",
                data: {
                  personPhone: personPhone
                },
                success: function(data) {
                    if (data.code === 100) {
                      console.log(data.data);
                      //printJS(data.data);
                      window.open(data.data, null);

                      {{--  var iframe = document.createElement('iframe');
                      iframe.style.display = 'none';
                      iframe.src = data.data;
                      console.log(iframe.src);
                      document.body.appendChild(iframe);
                      iframe.contentWindow.focus();
                      iframe.contentWindow.print();  --}}

                      {{--  var link=document.createElement('a');
                      document.body.appendChild(link);
                      link.href=data.data;
                      link.click();  --}}
                    }
                },
            });
            });
        });
    </script>
@endsection
