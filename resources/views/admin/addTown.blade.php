@extends('admin.layouts.template')
@section('page_title')
Ajouter ville - E-commerce
@endsection
@section('content')
<!-- Basic layout -->
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Admin/</span> Ajouter ville</h4>
    <div class="col-xxl">
        <div class="card mb-4">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h5 class="mb-0">Ajouter une nouvelle ville</h5>
            <small class="text-muted float-end"></small>
          </div>
          <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error )
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>

            @endif
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif

            @if (session()->has('danger'))
                <div class="alert alert-danger">
                    {{ session()->get('danger') }}
                </div>
            @endif
            <form action="{{route('storetown')}}" method="POST">
                @csrf
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="town_name">Nom ville <span class="text-danger">*<span></label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="town_name" name="town_name" placeholder="Saisissez le nom de la ville" />
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="code">Code ville</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="code" name="code" placeholder="Saisissez le code de la ville" />
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="shipping_cost">Frais de livraison</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" id="shipping_cost" name="shipping_cost" placeholder="Saisissez les frais de livraison" />
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="country_name">Nom pays</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="country_name" name="country_name" placeholder="Saisissez le nom du pays" />
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="district">Distric / Quartier</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="district" name="district" placeholder="Saisissez le nom du quartier" />
                </div>
              </div>

              <div class="row justify-content-end">
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Ajouter ville</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
</div>
@endsection
