@extends('admin.layouts.template')
@section('page_title')
Liste catégorie - E-commerce
@endsection
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Admin/</span> Liste catégories</h4>
    <!-- Bootstrapp Table whith header - Ligth -->

    @if (session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
    <div class="card">
        <h5 class="card-header">Catégories applicables</h5>
        <div class="card-datatable table-responsive text-nowrap">
          <table id="category-table" class="datatables table">
            <thead class="table-light">
              <tr>
                <th>N<sup>o</sup></th>
                <th>Nom catégorie</th>
                <th>Nombre sous-catégorie</th>
                <th>Quantité article</th>
                <th>slug</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody class="table-border-bottom-0">
                @foreach ($categories as $category)

              <tr>
                <td>{{ $category->{'id'} }}</td>
                <td>{{ $category->{'category_name'} }}</td>
                <td>{{ $category->{'subcategory_count'} }}</td>
                <td>{{ $category->{'product_count'} }}</td>
                <td>{{ $category->{'slug'} }}</td>
                <td>
                    <a href="{{ route('editcategory', $category->{'id'}) }}" class="btn btn-primary">Modifier</a>
                    <a href="{{ route('deletecategory', $category->{'id'}) }}" class="btn btn-warning">Supprimer</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
</div>
@endsection
@section('page-script')

    <script type="text/javascript">

        $(document).ready(function (){
            datatableInstance = $("#category-table").DataTable({
                "processing": true,
                "language": {
                "search": '',
                "searchPlaceholder": "Rechercher..."
                },
            });
        });
    </script>
@endsection
