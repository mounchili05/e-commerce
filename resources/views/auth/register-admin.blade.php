@extends('auth.layout')
@section('title')
Sauthentifier
@endsection

@section('content')
<!-- content -->
<div class="container" style="">
    <div style="text-align: center; margin-top: 10px;"><a href="{{route('home')}}">
            <h5><em>Retournez sur le site</em></h5>
        </a></div>
    <form class="mx-auto me-auto mt-1" action="{{route('registerstoreadmin')}}" method="post">
        @csrf
        @if (session()->has('message'))
        <div class="alert alert-info">
            {{ session()->get('message') }}
        </div>
        @endif
        <div class="container card" style="width: 400px; padding-top: 10px;">
            <div>
                <h6 style="text-align: center;">Enregistrez-vous sur notre site</h6>
            </div>
            <br>
            @include('alerts.alert-message')
            <div class="">
                <div class="form-group ">
                    <label for="name" class="form-label">Nom complet</label>
                    <input id="name" name="name" type="text" class="form-control">
                </div>
                <div class="form-group ">
                    <label for="email" class="form-label">Email</label>
                    <div class="input-group input-group-merge">
                        <input id="email" name="email" type="email" class="form-control">
                        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="form-label">Mot de passe</label>
                    <div class="input-group input-group-merge">
                        <input id="password" name="password" type="password" class="form-control">
                        <span class="input-group-text"><i class="fa fa-lock"></i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="confirmPassword" class="form-label">Confirmer mot de passe</label>
                    <div class="input-group input-group-merge">
                        <input id="password_confirmation" name="password_confirmation" type="password" class="form-control">
                        <span class="input-group-text"><i class="fa fa-lock"></i></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label"> Sélectionner un rôle</div>
                    <select class="form-select form-control" id="role_id" name="role_id" aria-label="Default select example">
                        <option selected>Sélectionnez un rôle</option>
                        @foreach ($roles as $role)
                        <option value="{{ $role->{'id'} }}">{{ $role->{'name'} }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary form-control">Enregistrer</button>
                    <a href="{{route('loginformadmin')}}">Déjà enregistré?</a>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
