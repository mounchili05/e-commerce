<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title', 'Connexion - enregistre')</title>
    <link rel="stylesheet"type="text/css" href="{{asset('home/css/bootstrap.min.css')}}">
     <!-- fonts -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap" rel="stylesheet">
      <!-- font awesome -->
      <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body style="background-image: url('../home/images/ecom-background.png');">

    @yield('content')
    <script src="{{asset('home/js/bootstrap.bundle.min.js')}}"></script>
</body>
</html>
