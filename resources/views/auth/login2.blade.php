@extends('auth.layout')
@section('title')
Sauthentifier
@endsection

@section('content')
<!-- content -->

<div class="container">
    <div style="text-align: center; margin-top: 70px;"><a href="{{route('home')}}"><h5><em>Retournez sur le site</em></h5></a></div>
    @props(['status'])

    @if ($status)
        <div {{ $attributes->merge(['class' => 'font-medium text-sm text-green-600']) }}>
            {{ $status }}
        </div>
    @endif
    <form class="mx-auto me-auto mt-2" action="{{route('login2')}}"  method="post">
        @csrf

        <div class="container card" style="width: 400px; padding-top: 20px;">
            <h6 class="mb-4" style="text-align: center;"><b>Connectez-vous pour demarrer une session</b></h6>

            @if (session()->has('message'))
            <div class="alert alert-info">
                <em>{{ session()->get('message') }}</em>
            </div>
            @endif
            @if (session()->has('danger'))
            <div class="alert alert-danger" style="text-align: center;">
                <em>{{ session()->get('danger') }}</em>
            </div>
            @endif
            @if($errors->any())
                <ul class="rounded alert-danger">
                    @foreach ($errors->all() as $error )
                        <li class="!text-danger">{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            <div class="">
                <div class="form-group ">
                    <label for="email" class="form-label">Email</label>
                    <div class="input-group input-group-merge">
                        <input id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Votre adresse email">
                        @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label id="password" for="" class="form-label">Mot de passe</label>
                    <div class="input-group input-group-merge">
                        <input id="password" name="password" type="password" class="form-control  @error('password') is-invalid @enderror" aria-describedby="emailHelp" placeholder="Votre mot de passe">
                        @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <span class="input-group-text"><i class="fa fa-lock"></i></span>
                    </div>
                </div>
                <div class="form-group mx-4">
                    <input id="checkbox" type="checkbox" class="form-check-input" aria-describedby="emailHelp">
                    <label for="check" class="form-check-label">Se souvenir de moi</label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary form-control">Se connecter</button>
                    @if (Route::has('passwordrequest'))
                        <a class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" href="{{ route('passwordrequest') }}">
                            {{ __('Mot de passe oublié?') }}
                        </a>
                    @endif
                </div>
                <!--<a href="{{route('passwordrequest')}}"><em>Mot de passe oublié?</em></a>-->
                <p>Vous n'avez pas encore de compte?<a href="{{route('register2')}}"><em> S'inscrire</a></em> </p>
            </div>
        </div>
    </form>
</div>
@endsection
