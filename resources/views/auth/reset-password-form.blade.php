@extends('auth.layout')
@section('title')
Changer le mot de passe
@endsection

@section('content')
<!-- content -->

<div class="container">
    <div style="text-align: center; margin-top: 70px;"><a href="{{route('home')}}"><h5><em>Retournez sur le site</em></h5></a></div>

    <form class="mx-auto me-auto mt-2" id="resetPasswordForm" class="mb-3" action="{{ route('reset-password') }}" method="POST" autocomplete="off">
        @csrf
        <div class="container card" style="width: 400px; padding-top: 20px;">
            <h5 class="mb-2" style="text-align: center;">Réinitialiser votre mot de passe</h5>
            <p class="mb-4" style="text-align: justify;">Saisissez votre nouveau mot de passe et confirmez-le pour terminer.</p>
            @if (session()->has('message'))
            <div class="alert alert-info mx-auto me-auto text-center" style="width: 400px;">
                <em>{{ session()->get('message') }}</em>
            </div>
            @endif
            @include('alerts.alert-message')
            <input type="hidden" value="{{ $email }}" name="email">
            <div class="mb-2 form-password-toggle">
                <label class="form-label" for="_password">Mot de passe</label>
                <div class="input-group input-group-merge">
                    <input type="password" id="_password" value="{{ old('password') }}" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Entrez votre nouveau mot de passe" aria-describedby="password"/>
                    <span class="input-group-text cursor-pointer"><i class="fa fa-lock"></i></span>
                </div>
                @error('password')
                    <span class="invalid-feedback d-block">
                    {{ $message }}
                    </span>
                @enderror
            </div>
            <div class="mb-4 form-password-toggle">
                <label class="form-label" for="_confirm-password">Confirmez votre mot de passe</label>
                <div class="input-group input-group-merge">
                    <input type="password" id="_confirm-password" value="{{ old('confirm-password') }}" class="form-control @error('confirm-password') is-invalid @enderror" name="confirm-password" placeholder="Confirmez votre mot de passe" aria-describedby="password"/>
                    <span class="input-group-text cursor-pointer"><i class="fa fa-lock"></i></span>
                </div>
                @error('confirm-password')
                    <span class="invalid-feedback d-block">
                    {{ $message }}
                    </span>
                @enderror
            </div>
            <div class="mb-1">
            <button id="btnResetPassword" class="btn btn-primary d-grid w-100" type="submit">Changer mot de passe</button>
            </div>
        </form>

        <p class="text-center mt-0 mb-2">
            <a href="{{ route('login') }}"><span>Allez à la page de connexion</span></a>
        </p>
    </div>
</div>
@endsection
