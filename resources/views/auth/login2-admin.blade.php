@extends('auth.layout')
@section('title')
Sauthentifier
@endsection

@section('content')
<!-- content -->

<div class="container">
    <div style="text-align: center; margin-top: 70px;"><a href="{{route('home')}}"><h5><em>Retournez sur le site</em></h5></a></div>
    <form class="mx-auto me-auto mt-2" action="{{route('login2')}}"  method="post">
        @csrf
        @if (session()->has('message'))
        <div class="alert alert-info mx-auto me-auto text-center" style="width: 400px;">
            <em>{{ session()->get('message') }}</em>
        </div>
        @endif
        <div class="container card" style="width: 400px; padding-top: 50px;">
            <div class="row mx-2"><h6>Connectez-vous pour demarrer une session</h6></div>
            <br>
            @include('alerts.alert-message')
            <div class="">
                <div class="form-group ">
                    <label for="email" class="form-label">Email</label>
                    <div class="input-group input-group-merge">
                        <input id="email" name="email" type="email" class="form-control" @error('email') is-invalid @enderror" placeholder="Votre adresse email" }}">
                        @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label id="password" for="" class="form-label">Mot de passe</label>
                    <div class="input-group input-group-merge">
                        <input id="password" name="password" type="password" class="form-control" aria-describedby="emailHelp" @error('password') is-invalid @enderror" placeholder="Votre mot de passe" }}">
                        @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <span class="input-group-text"><i class="fa fa-lock"></i></span>
                    </div>
                </div>
                <div class="form-group mx-4">
                    <input id="checkbox" type="checkbox" class="form-check-input" aria-describedby="emailHelp">
                    <label for="check" class="form-check-label">Se souvenir de moi</label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary form-control">Se connecter</button>
                </div>
                <a href=""><em>Mot de passe oublié?</em></a>
                <p>Vous n'avez pas encore de compte?<a href="{{route('registeradmin')}}"><em> S'inscrire</a></em> </p>
            </div>
        </div>
    </form>
</div>
@endsection
