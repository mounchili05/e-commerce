@extends('auth.layout')
@section('title')
Mot de passe oublié
@endsection

@section('content')
<!-- content -->

<div class="container">
    <div style="text-align: center; margin-top: 70px;"><a href="{{route('home')}}"><h5><em>Retournez sur le site</em></h5></a></div>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
    <form class="mx-auto me-auto mt-2" action="{{route('passwordemailstore')}}"  method="post">
        @csrf
        @if (session()->has('message'))
        <div class="alert alert-info mx-auto me-auto text-center" style="width: 400px;">
            <em>{{ session()->get('message') }}</em>
        </div>
        @endif
        <div class="container card" style="width: 400px; padding-top: 20px;">
            <div><h6 style="text-align: center;">Réinitialiser votre mot de passe</h6></div>
            <p style="text-align: justify;">Mot de passe oublié? Aucun problème. Indiquez-nous simplement votre adresse e-mail et nous vous enverrons par e-mail un lien de réinitialisation de mot de passe qui vous permettra den choisir un nouveau.</p>
            <br>
            @include('alerts.alert-message')
            <div class="">
                <div class="form-group ">
                    <label for="email" class="form-label">Email</label>
                    <div class="input-group input-group-merge">
                        <input id="email" name="email" value="@if(Session::has('old_email')) {{Session::get('old_email')}} @endif" type="email"
                        class="form-control @error('email-success') is-valid @enderror @error('email-error') is-invalid @enderror"
                        placeholder="{{"Votre adresse email" }}" required autofocus>
                        @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary form-control">Lien réinitialisation mot de passe</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
