@extends('user_template.layouts.template')
@section('main-content')
<h2 class=""><strong>Contacter nous</strong></h2>

@if (session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
<div class="col-7">
    <div class="box_main">
        <form action="{{route('contactusstore')}}" method="post">
            @csrf
            <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Nom complet</label>
                <div class="col-sm-10">
                    <input type="text" name="name" id="name" class="form-control" placeholder="Nom complet">
                </div>
            </div>

            <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Email</label>
                <div class="col-sm-10">
                    <input type="text" name="email" id="email" class="form-control" placeholder="email">
                </div>
            </div>

            <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Sujet</label>
                <div class="col-sm-10">
                    <input type="text" name="subject" id="subject" class="form-control" placeholder="Sujet">
                </div>
            </div>

            <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Message</label>
                <div class="col-sm-10">
                    <textarea name="message" id="message" cols="30" rows="6" class="form-control" placeholder="Votre message"></textarea>
                </div>
            </div>

            <div class="row justify-content-end">
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Envoyer</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
