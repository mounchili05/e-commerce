@extends('user_template.layouts.template')
@section('main-content')
    <!-- fashion section start -->
    <h2 class="py-5"><strong>Recherche</strong></h2>
    <div class="fashion_section">
        <div id="main_slider">
                <div class="container">
                    @if(request()->input('q'))
                        <h6>{{ $products->total() }} résultat(s) pour la recherche " {{ request()->q}} " </h6>
                    @endif
                    <h1 class="fashion_taital">Résultat recherche</h1>
                    <div class="fashion_section_2">
                    <div class="row">
                        @foreach ($products as $product )
                        <div class="col-lg-3 col-sm-3">
                            <div class="box_main">
                                <h4 class="shirt_text">{{ $product->{'product_name'} }}</h4>
                                <p style="font-weight: bold;">Prix :  <span class="price_text">{{ number_format($product->{'price'}, 2, ',', '.') }} XAF</span></p>
                                <p class="text mb-1" style="height: 90px; text-align: justify">{{ $product->{'product_short_desc'} }} </p>
                                <div class="tshirt_img"><img style="height: 120px;" src="{{asset($product->{'product_img'})}}"></div>
                                <div class="btn_main">
                                <div class="buy_bt">
                                    <form action="{{route('addproducttocart')}}" method="post">
                                        @csrf
                                        <input type="hidden" value="{{ $product->{'id'} }}" name="product_id">
                                        <input type="hidden" value="{{ $product->{'price'} }}" name="price">
                                        <input type="hidden" value="1" name="quantity">

                                        <input class="btn btn-warning" type="submit" value="Acheter">
                                    </form>
                                </div>
                                <div class="seemore_bt"><a href="{{route('singleproduct', [$product->{'id'}, $product->{'slug'}])}}">Voir plus...</a></div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                    </div>
                </div>
                {{$products->appends(request()->input())->links() }}
            </div>
      </div>
@endsection
