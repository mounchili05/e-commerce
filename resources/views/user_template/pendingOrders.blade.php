@extends('user_template.layouts.user_profile_template')
@section('profilecontent')
<h2>Commandes en attente</h2>
@if (session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

<table class="table">
    <tr>
        <td>Nom article</td>
        <td>Quantité</td>
        <td>Prix</td>
        @foreach ($pendingOrders as $order)
            <tr>
                <td>{{ $order->{'product_name'} }}</td>
                <td>{{ $order->{'quantity'} }}</td>
                <td>{{ $order->{'total_price'} }}</td>
            </tr>
        @endforeach
    </tr>
</table>
@endsection
