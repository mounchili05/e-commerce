@php
    $categories = App\Models\Category::latest()->get();
    $subCategories = App\Models\SubCategory::latest()->get();
@endphp

<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>E-commerce</title>

      @yield('extra-script')
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">

      @yield('extra-meta')
      <!-- bootstrap css -->
      <link rel="stylesheet" type="text/css" href="{{asset('home/css/bootstrap.min.css')}}">
      <!-- style css -->
      <link rel="stylesheet" type="text/css" href="{{asset('home/css/style.css')}}">
      <!-- Responsive-->
      <link rel="stylesheet" href="{{asset('home/css/responsive.css')}}">
      <!-- fevicon -->
      <link rel="icon" href="{{asset('home/images/fevicon.png')}}" type="image/gif" />
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="{{asset('home/css/jquery.mCustomScrollbar.min.css')}}">
      <!-- Tweaks for older IEs-->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
      <!-- fonts -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap" rel="stylesheet">
      <!-- font awesome -->
      <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <!--  -->
      <!-- owl stylesheets -->
      <link href="https://fonts.googleapis.com/css?family=Great+Vibes|Poppins:400,700&display=swap&subset=latin-ext" rel="stylesheet">
      <link rel="stylesheet" href="{{asset('home/css/owl.carousel.min.css')}}">
      <link rel="stylesoeet" href="{{asset('home/css/owl.theme.default.min.css')}}">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">

    <!-- Icons -->
    <link rel="stylesheet" href="{{asset('home/fonts/fontawesome.css')}}" />
    <link rel="stylesheet" href="{{asset('home/fonts/flag-icons.css')}}" />


   </head>
   <body>
      <!-- banner bg main start -->
      <div class="banner_bg_main">
         <!-- header top section start -->
         <div class="container">
            <div class="header_section_top">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="custom_menu">
                        <ul>
                           <li><a href="#">Meilleures ventes</a></li>
                           <li><a href="#">Articles en promotion</a></li>
                           <li><a href="{{route('newrelease')}}">Services livraison</a></li>
                           <li><a href="{{route('todaysdeal')}}">Suggestions</a></li>
                           <li><a href="{{route('customerservice')}}">Service client</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- header top section start -->
         <!-- logo section start -->
         <div class="logo_section">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="logo"><a href="{{route('home')}}"> <h1 class="text-primary"> <strong>CHOCHO MARKET</strong></h1><!--<img src="{{asset('home/images/logo.png')}}">--></a>
                    <h2 class="text-center"><i class="badge badge-secondary">Achat, acheminement sécurisé et livraison gratuite et expresse</i></h2>
                    </div>
                </div>
               </div>
            </div>
         </div>
         <!-- logo section end -->
         <!-- header section start -->
         <div class="header_section">
            <div class="container">
               <div class="containt_main">
                  <div id="mySidenav" class="sidenav">
                     <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                     <a href="#">Catégories</a>
                    @foreach ($categories as $category )
                        <a href="{{route('category', [$category->{'id'}, $category->{'slug'}])}}">{{ $category->{'category_name'} }}</a>
                    @endforeach
                    <div class="m-3" style="height: 1px; background-color: #ff7d95;"><hr></div>
                    @if(!auth()->guest())
                        <form action="{{route('logout')}}" method="post" id="deconnexion2">
                            @csrf
                            <a href="javascript:void(0);" id="liendeconnexion2" class="dropdown-item" style="color: black;"><i class="fa fa-sign-out" aria-hidden="true"></i> Deconnexion</a>
                        </form>
                    @else
                        <a href="{{route('login')}}">
                            <span class="padding_10"><i class="fa fa-sign-in" aria-hidden="true"></i> Se connecter</span>
                        </a>
                    @endif
                  </div>
                  <span class="toggle_icon" onclick="openNav()"><img src="{{asset('home/images/toggle-icon.png')}}"></span>
                  <div class="dropdown">
                     <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Toutes les sous-catégories
                     </button>
                     <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach ($subCategories as $category )
                            <a class="dropdown-item" href="{{route('subcategory', [$category->{'id'}, $category->{'slug'}])}}">{{ $category->{'subcategory_name'} }}</a>
                        @endforeach
                     </div>
                  </div>
                  <div class="main">
                     <!-- Another variation with a button -->
                     <div class="input-group">

                        <form action="{{route('productssearch')}}" class="d-flex rm-3">
                            <div class="form-group mb-0 mr-1">
                                <input type="text" name="q" class="form-control" placeholder="Recherche..." value="{{request()->q ?? ''}}">
                            </div>
                            <button class="btn btn-secondary" type="submit" style="background-color: #f26522; border-color:#f26522 ">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </form>
                     </div>
                  </div>
                  <div class="header_box">
                     <div class="lang_box ">
                        <a href="#" title="Language" class="nav-link" data-toggle="dropdown" aria-expanded="true">
                            <img src="{{asset('home/images/flag-france.png')}}" class="mr-2" alt="flag" title="Français"> Français <i class="fa fa-angle-down ml-2" aria-hidden="true"></i>
                        </a>
                        <div class="dropdown-menu">
                           <a href="#" class="dropdown-item">
                            <img src="{{asset('home/images/flag-uk.png')}}" alt="flag" class="mr-2 " title="United Kingdom"> English
                           </a>
                        </div>
                     </div>
                     <div class="">
                        <a href="{{route('cartindex')}}" style="font-weight: bold;">
                            <i class="fa fa-shopping-cart mr-1" aria-hidden="true"></i>Panier <span class="badge badge-pill badge-dark"> {{ Cart::count() }}</span>
                        </a>
                     </div>

                     <div class="login_menu">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-user mr-1" aria-hidden="true"></i> User/Admin</button>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a href="{{route('register2')}}" class="dropdown-item"><span><i class="fa fa-shield" aria-hidden="true"></i> Senregistrer</span>
                                </a>
                                @if(!auth()->guest())
                                <form action="{{route('logout')}}" method="get" id="deconnexion">
                                    @csrf
                                    <a href="javascript:void(0);" id="liendeconnexion" class="dropdown-item"><span><i class="fa fa-sign-out" aria-hidden="true"></i> Deconnexion</span>
                                    </a>
                                </form>
                                @else
                                    <a href="{{route('login')}}" class="dropdown-item">
                                        <span><i class="fa fa-sign-in" aria-hidden="true"></i> Se connecter</span>
                                    </a>
                                @endif
                            </div>
                        </div>

                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- header section end -->

         <!-- banner section start -->
         <div class="banner_section layout_padding">
            <div class="container">
               <div id="my_slider" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                     <div class="carousel-item active">
                        <div class="row">
                           <div class="col-sm-12">
                              <h1 class="banner_taital">Bienvenue <br>à CHOCHO MARKET</h1>
                           </div>
                        </div>
                     </div>
                     <div class="carousel-item">
                        <div class="row">
                           <div class="col-sm-12">
                              <h1 class="banner_taital">Commencez <br>Vos achats préférés</h1>
                           </div>
                        </div>
                     </div>
                     <div class="carousel-item">
                        <div class="row">
                           <div class="col-sm-12">
                              <h1 class="banner_taital">CHOCHO MARKET <br>des prix imbattables</h1>
                           </div>
                        </div>
                     </div>
                  </div>

               </div>
            </div>
         </div>
         <!-- banner section end -->

      </div>
      <!-- banner bg main end -->
      <!-- Common part -->

      <div class="container py-5" style="margin-top: 200px;">
        @yield('main-content')
        <!-- BEGIN: Page JS-->
        @yield('page-script')
        <!-- END: Page JS-->
      </div>
      <!-- End common part -->

      <!-- footer section start -->
      <div class="footer_section layout_padding">
         <div class="container">
            <div class="footer_logo"><a href="index.html"><img style="height: 100px;" src="{{asset('home/images/ventes.png')}}"></a></div>
            <div class="input_bt">
               <input type="text" class="mail_bt" placeholder="Votre Email" name="email">
               <span class="subscribe_bt" id="basic-addon2"><a href="#">Abonner vous</a></span>
            </div>
            <div class="footer_menu">
               <ul>
                    <li><a href="#">A propos</a></li>
                    <li><a href="{{route('contactus')}}">Nous contacter</a></li>
                    <li><a href="{{route('newrelease')}}">Nouvelles versions</a></li>
                    <li><a href="{{route('todaysdeal')}}">Les transactions du jour</a></li>
                    <li><a href="{{route('customerservice')}}">Service client</a></li>
               </ul>
            </div>
            <div class="location_main">Nous contacter au numéro : <a href="#">+237 655 09 23 85</a></div>
         </div>
      </div>

      <!-- footer section end -->
      <!-- copyright section start -->
      <div class="copyright_section">
         <div class="container">
            <p class="copyright_text">© {{date('Y')}} All Rights Reserved. Design by <a href="#">César</a></p>
         </div>
      </div>
      <!-- copyright section end -->
      <!-- Javascript files-->
      <script src="{{ asset('home/js/jquery.min.js')}}"></script>
      <script src="{{asset('home/js/popper.min.js')}}"></script>
      <script src="{{asset('home/js/bootstrap.bundle.min.js')}}"></script>
      <script src="{{asset('home/js/jquery-3.0.0.min.js')}}"></script>
      <script src="{{asset('home/js/plugin.js')}}"></script>
      <!-- sidebar -->
      <script src="{{asset('home/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
      <script src="{{asset('home/js/custom.js')}}"></script>
      <script>
         function openNav() {
           document.getElementById("mySidenav").style.width = "250px";
         }

         function closeNav() {
           document.getElementById("mySidenav").style.width = "0";
         }

        document.addEventListener("mouseup", function(event) {
            var obj = document.getElementById("liendeconnexion");
            if (obj.contains(event.target)) {
                $("#deconnexion").submit();
            }
        });

        document.addEventListener("mouseup", function(event) {
            var obj = document.getElementById("liendeconnexion2");
            if (obj.contains(event.target)) {
                $("#deconnexion2").submit();
            }
        });

      </script>
      @yield('extra-js')
   </body>
</html>
