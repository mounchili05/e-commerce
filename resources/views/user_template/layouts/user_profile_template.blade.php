@extends('user_template.layouts.template')
@section('main-content')
<div class="container">
    <div class="row">
        <div class="col-lg-4">
            <div class="box_main">
                <ul>
                    <li><a href="{{route('userprofile')}}">Tableau de bord</a></li>
                    <li><a href="{{route('pendingorders')}}">Commande en cours</a></li>
                    <li><a href="{{route('history')}}">Historique</a></li>
                    <li><a href=""><span><i class="fa fa-power-of" aria-hidden="true"></i> Deconnexion</span></a></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="box_main">
                @yield('profilecontent')
            </div>
        </div>
    </div>
</div>
@endsection


