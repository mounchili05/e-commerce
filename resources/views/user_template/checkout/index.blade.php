@extends('user_template.layouts.template')

@section('extra-script')
<script src="https:://js.stripe.com/v3/"></script>
@endsection

@section('main-content')
<div class="col-md-12">
    <h2 style="font-weight: bold;">Page de paiement</h2>
    <div class="row">
        <div class="box_main col-md-8">
            <form action="#" class="my-4">
                <div id="card-element" class="alert alert-info p-2" style="text-align: justify;">
                    <h1><i>Nous rencontrons un petit soucis avec notre agrégateur de solution de paiement en ligne pour le moment,
                       mais vous pouvez toujours nous contacter au numéros de téléphone ci-dessous et effectuer votre paiement
                       Orange money (OM) ou Mtn mobile money (MOMO).</i>
                    </h1>
                    <h2> +237 674816238 / +237 696470157</h2>

                </div>

                <div id="card-errors" role="alert"></div>

                <button class="btn btn-success mt-4" id="submit" disabled="disabled">procéder au paiement</button>
            </form>
        </div>
    </div>
</div>

@endsection
@section('extra-js')

<script>
    var stripe = stripe('pk_test_NKUtAyXSsBBCCJtarnWmLgZgZf00GDHArOnM');
    var elements = stripe.elements();

    var style = {
        base: {
            color: "#32325d",
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: "antialiased",
            fontSize: "16px",
            "::placeholder": {
                color: "#aab7c4"
            }
        },
        invalid: {
            color: "#fa755a",
            iconColor: "#fa755a"
        }
    };

    var card = elements.create("card", {style: style});
    card.mount("#card-element");
    card.addEventListener('change', ({error}) => {
        const displayError = document.getElementById('card-errors');
        if(error){
            displayError.classList.add('alert', 'alert-warning');
            displayError.textContent = error.message;
        }else{
            displayError.classList.remove('alert', 'alert-warning');
            displayError.textContent = '';
        }
    });

    var submitButton = document.getElementById('submit');

    submitButton.addEventListener('click', function(ev){
        ev.preventDefault();
        stripe.confirmCardPayment("{{ $clientSecret }}", {
            payment_method: {
                card: card,
            }
        }).then(function(result){
            if(result.error){
                console.log(result.error.message);
            }else{
                if(result.paymentIntent.status === 'succeeded'){

                    console.log(result.paymentIntent);
                }
            }
        });
    });
</script>
@endsection
