@extends('user_template.layouts.template')
@section('main-content')
<h2 style="font-weight: bold;">Panier</h2>
@if (session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="row">
    <div class="col-12">
        <div class="box_main mt-2">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Photo article</th>
                        <th>Nom article</th>
                        <th>Quantité</th>
                        <th>Prix</th>
                        <th>Action</th>
                    </tr>
                    @php
                        $total = 0;
                    @endphp
                    @foreach ($cartItems as $cartItem)
                        @php
                            $productName = App\Models\Product::where('id', $cartItem->{'product_id'})->value('product_name');
                            $productImg = App\Models\Product::where('id', $cartItem->{'product_id'})->value('product_img');
                        @endphp
                        <tr>
                            <td><img src="{{asset($cartItem->{'product'}->{'product_img'})}}" style="height: 50px;" alt=""></td>
                            <td>{{ $cartItem->{'product'}->{'product_name'} }}</td>
                            <td>
                                <input type="hidden" class="form-control" min="1" style="width: 100px; background-color:lightgrey" name="quantity" id="quantity" value="1">
                                <input type="hidden" id="unitPrice" value="{{ $cartItem->{'price'} }}">
                                <input type="hidden" id="total1" value="{{ $total }}">
                            </td>
                            <td>{{ number_format($cartItem->{'price'}, 2, '.', ',') }} XAF</td>
                            <td><a href="{{route('removecartitem', $cartItem->{'id'})}}" class="btn btn-warning"><i class="fa fa-remove mr-1" aria-hidden="true"></i>Retirer</a></td>
                        </tr>
                        @php
                        $total = $total + $cartItem->{'price'};
                        @endphp
                    @endforeach
                        @if ($total > 0)
                        <tr>
                            <td></td>
                            <td></td>
                            <td><strong>Total</strong></td>
                            <td class="text-success"><strong>{{number_format($total, 2, '.', ',')}} XAF</strong></td>
                            <td><a href="{{route('shippingaddress')}}" class="btn btn-primary">Passer à la caisse</a></td>
                        </tr>
                        @else
                        <div class="alert alert-danger">
                            <h3>Aucun article trouvé dans le panier !</h3>
                        </div>
                        @endif
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-script')

<script type="text/javascript">

    {{--  $(document).ready(function (){

    });  --}}
</script>
@endsection
