@extends('user_template.layouts.template')
@section('main-content')
<h2>Indiquer vos informations de livraison</h2>
@if (session()->has('message'))
    <div class="alert alert-danger">
        {{ session()->get('message') }}
    </div>
@endif

@if (session()->has('danger'))
<div class="alert alert-danger">
    {{ session()->get('danger') }}
</div>
@endif
<div class="row mt-2">
    <div class="col-12">
        <div class="box_main p-5">
            <form action="{{route('addshippingaddress')}}" method="post">
                @csrf

                <div class="form-group">
                    <label for="name">Nom complet <span class="text-danger">*</span></label>
                    <input type="text" name="name" class="form-control">
                </div>

                <div class="form-group">
                    <label for="phone_number">Numéro de téléphone <span class="text-danger">*</span></label>
                    <input type="text" name="phone_number" class="form-control">
                </div>

                <div class="form-group">
                    <label for="city_name">Ville - quartier - secteur <span class="text-danger">*</span></label>
                    <input type="text" name="city_name" class="form-control">
                </div>
                <div class="form-group">
                    <label class="form-label" for="town">Lieu de livraison <span class="text-danger">*</span></label>
                    <div class="">
                        <select class="form-control form-select" id="placeOfDelivery" name="district">
                            <option value=""> -- Sélectionnez un lieu de livraison --</option>
                            @foreach ($towns as $town)
                            <option value="{{ $town->{'district'} }}">{{ $town->{'district'} }}</option>
                            @endforeach
                            </select>
                    </div>
                </div>

                <div class="form-group badge badge-info hide-when-no-click" style="display: none;">
                    <i><h4>Pour ce lieu de livraison, les frais de livraison seront de :
                        <span style="color: red; font-weight: bold;" id="shippingCostInfo"> </span>
                    </h4></i>
                </div>

                <div class="form-group">
                    <label for="postal_code">Boîte postale</label>
                    <input type="text" name="postal_code" class="form-control">
                </div>

                <input type="submit" value="Enregistrer" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
@endsection
@section('page-script')

<script src="{{ asset('js/jquery-3.7.1.min.js') }}"></script>
<script type="text/javascript">

$(document).ready(function (){
    $("#placeOfDelivery").on("change", function(){
        let district = $("#placeOfDelivery").val();

        if($(this).val() != ""){
            $.ajax({
                url: "{{ route('getshippingcost')}}",
                data:{
                    district: district,
                },
                type: "get",
                dataType: "json",
                success: function(data){
                    if(data.code === 100){
                        console.log(data.data);
                        $(".hide-when-no-click").show();
                        $("#shippingCostInfo").html(data.data);
                    }
                },
                error: function(){},
            });
        }
    });

});
</script>
@endsection
