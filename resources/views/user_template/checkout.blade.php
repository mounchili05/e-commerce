@extends('user_template.layouts.template')
@section('main-content')
<h2 class="text-success"><strong>Dernière étape pour passer votre commande</strong></h2>
@if (session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

@if (session()->has('danger'))
    <div class="alert alert-danger">
        {{ session()->get('danger') }}
    </div>
@endif
<div class="row mt-2">
    <div class="col-7">
        <div class="box_main">
            <h3><strong>Le produit sera livré à -</strong></h3>
            <p>Ville - quartier - secteur : - <strong>{{ $shippingAddress->{'city_name'} }}</strong></p>
            <p>Boîte postale - <strong>{{ $shippingAddress->{'postal_code'} }}</strong></p>
            <p>Numéro de téléphone - {{ $shippingAddress->{'phone_number'} }}</p>
        </div>
    </div>

    <div class="col-5">
        <div class="box_main">
            <h3><strong>Vos produits finaux sont</strong></h3>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Nom article</th>
                        <th>Quantité</th>
                        <th>Prix</th>
                    </tr>
                    @php
                        $total = 0;
                    @endphp
                    @foreach ($cartItems as $cartItem)
                        @php
                            $productName = App\Models\Product::where('id', $cartItem->{'product_id'})->value('product_name');
                        @endphp
                        <tr>
                            <td>{{ $cartItem->{'product'}->{'product_name'} }}</td>
                            <td>{{ $cartItem->{'quantity'} }}</td>
                            <td>{{ number_format($cartItem->{'price'}, 2, '.', ',') }} XAF</td>
                        </tr>
                        @php
                        $total = $total + $cartItem->{'price'};
                        @endphp
                    @endforeach
                    <tr>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td class="text-success"><strong>{{number_format($total, 2, '.', ',')}} XAF</strong></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <form action="" method="post">
        @csrf
        <input type="submit" name="" value="Annuler la commande" class="btn btn-danger mr-3">
    </form>
    <form action="{{route('placeorder')}}" method="post">
        @csrf
        <input type="submit" name="" value="payer" class="btn btn-primary">
    </form>
</div>
@endsection
