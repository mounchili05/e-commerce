@extends('user_template.layouts.template')

@section('extra-meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('main-content')
<h2 style="font-weight: bold;">Panier</h2>
@if (session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

@if (session()->has('danger'))
    <div class="alert alert-danger">
        {{ session()->get('danger') }}
    </div>
@endif
@if(Cart::count() > 0)
<div class="row">
    <div class="col-12">
        <div class="box_main mt-2">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Photo article</th>
                        <th>Nom article</th>
                        <th>Quantité</th>
                        <th>Prix</th>
                        <th>Retirer du panier</th>
                    </tr>
                    @php
                        $total = 0;
                    @endphp
                    @foreach (Cart::content() as $cartItem)

                        @php
                            $productName = App\Models\Product::where('id', $cartItem->id)->value('product_name');
                            $productImg = App\Models\Product::where('id', $cartItem->id)->value('product_img');
                            $quantity = App\Models\Product::where('id', $cartItem->id)->value('quantity');
                        @endphp
                        <tr>
                            <td><img src="{{$productImg }}" style="height: 50px;" alt=""></td>
                            <td>{{ $productName }}</td>
                            <td>
                                <select name="qty" id="qty" data-max="{{ $quantity }}" data-id="{{ $cartItem->rowId }}" class="custom-select">
                                    @for ($i = 1; $i<= $quantity; $i++)
                                        <option value="{{$i}}" {{$i == $cartItem->qty ? 'selected' : ''}}>{{$i}}</option>
                                    @endfor
                                </select>
                                <input type="hidden" id="unitPrice" value="{{ $cartItem->{'price'} }}">
                                <input type="hidden" id="total1" value="{{ $total }}">
                            </td>
                            <td>{{getPrice($cartItem->subtotal()) }} </td>
                            <td>
                                <form action="{{route('cartremove', $cartItem->rowId) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="text-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>

                                </form>
                            </td>
                        </tr>
                        @php
                        $total = $total + $cartItem->{'price'} * $cartItem->qty;
                        @endphp
                    @endforeach
                        @if ($total > 0)
                        <tr>
                            <td></td>
                            <td></td>
                            <td><strong>Total</strong></td>
                            <td class="text-success"><strong>{{getPrice($total) }} </strong></td>
                            <td></td>
                            {{--  <td><a href="{{route('shippingaddress')}}" class="btn btn-primary">Passer à la caisse</a></td>  --}}
                        </tr>
                        @else
                        <div class="alert alert-danger">
                            <h3>Aucun article trouvé dans le panier !</h3>
                        </div>
                        @endif
                </table>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-6 box_main">
    <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Détail de la commande</div>
    <div class="p-4">
        <p class="font-italic mb-4">les frais dexpédition et supplémentaires sont calculés en fonction des valeurs que vous avez saisies </p>
        <ul class="list-unstyled mb-4">
            <li class="d-flex justify-content-between py-3 border-bottom">
                <strong class="text-muted">Sous-total</strong><strong>{{getPrice(Cart::subtotal()) }}</strong>
            </li>
            <li class="d-flex justify-content-between py-3 border-bottom">
                <strong class="text-muted">Taxe</strong><strong>{{getPrice(Cart::tax()) }}</strong>
            </li>
            <li class="d-flex justify-content-between py-3 border-bottom">
                <strong class="text-muted">Total</strong><h5 class="font-weight-bold">{{getPrice(Cart::total()) }}</strong>
            </li>
        </ul>
        <a href="{{route('checkoutindex')}}" class="btn btn-dark rounded-pill py-2 btn-block"><i class="fa fa-credit-card mr-1" aria-hidden="true"></i>Passer à la caisse</a>
        <a href="{{route('shippingaddress')}}" class="btn btn-warning rounded-pill py-2 btn-block"><i class="fa fa-tags mr-1" aria-hidden="true"></i>Commander</a>
    </div>
</div>
@else
<div class="col-md-12 alert alert-danger">
    <h3>Votre panier est vide !</h3>
</div>
@endif
@endsection
@section('page-script')

<script>

    var selects = document.querySelectorAll("#qty");
    Array.from(selects).forEach((element) =>{
        element.addEventListener('change', function(){
            var rowId = this.getAttribute('data-id');
            var qtyMax = this.getAttribute('data-max');
            var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            console.log(rowId);
            fetch(
                `/cart-update/${rowId}`,
                {
                    headers:{
                        "Content-Type": "application/json",
                        "Accept": "application/json, text-plain, */*",
                        "X-Requested-With": "XMLHttpRequest",
                        "X-CSRF-TOKEN": token
                    },
                    method: 'post',
                    body: JSON.stringify({
                        qty: this.value,
                        qtyMax: qtyMax
                    })
                }
            ).then((data) =>{
            console.log(data);
            location.reload();
            }).catch((error) =>{
            console.log(error);
            })
        });
    });
    {{--  $(document).ready(function (){

    });  --}}
</script>
@endsection
