@extends('user_template.layouts.template')
@section('main-content')
<div class="container">
    <div class="row">
        <div class="col-lg-4 mt-4">
            <div class="box_main">
                <div class="tshirt_img"><img src="{{asset($product->{'product_img'})}}"></div>
            </div>
        </div>
        <div class="col-lg-8 mt-4"">
            <div class="box_main">
                <div class="poduct-info">
                    <h4 class="shirt_text text-left">{{ $product->{'product_name'} }}</h4>
                    <p class="price_text text-left"><span style="font-weight: bold;">Prix :</span> {{ getPrice($product->{'price'}) }}</p>
                </div>
                <div class="my-3 product-details">
                    <p class="lead">{{ $product->{'product_desc'} }}</p>
                    <ul class="p-2 bg-light my-2">
                        <li><span style="font-weight: bold;">Catégorie :</span> {{ $product->{'product_category_name'} }}</li>
                        <li><span style="font-weight: bold;">Sous-catégorie :</span> {{ $product->{'product_subcategory_name'} }}</li>
                        <li><span style="font-weight: bold;">Quantité disponible :</span> {{ $product->{'quantity'} }}</li>
                    </ul>
                </div>

                <div class="btn_main">
                    <form action="{{route('cartstore')}}" method="post">
                        @csrf
                        <input type="hidden" value="{{ $product->{'id'} }}" name="product_id">
                        <div class="form-group">
                            {{--  <input type="hidden" value="{{ $product->{'product_name'} }}" name="product_name">
                            <input type="hidden" value="{{ $product->{'price'} }}" name="price">  --}}
                            {{--  <label for="quantity" class="form-label"><span style="font-weight: bold;">Quelle quantité ?</span></label>
                            <input class="form-control" type="number" min="1" max="{{$product->{'quantity'} }}" placeholder="1" name="quantity" value="1">  --}}
                        </div>
                        <br />
                        <div class="input-group">
                            <button class="btn btn-secondary" type="submit"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Ajouter</button>
                        </div>
                    </form>
                 </div>
            </div>
        </div>
    </div>

    <div class="fashion_section">
        <div id="main_slider">
            <div class="carousel-item active">
                <div class="container">
                    <h1 class="fashion_taital">Articles similaires</h1>
                    <div class="fashion_section_2">
                        <div class="row">
                            @foreach ($relatedProducts as $product)
                            <div class="col-lg-4 col-sm-4">
                                <div class="box_main">
                                    <h4 class="shirt_text">{{ $product->{'product_name'} }}</h4>
                                    <p style="font-weight: bold;">Prix :  <span class="price_text">{{ getPrice($product->{'price'}) }} </span></p>
                                    <p class="text mb-1" style="height: 90px; text-align: justify">{{ $product->{'product_short_desc'} }} </p>
                                    <div class="tshirt_img"><img style="height: 120px;" src="{{asset($product->{'product_img'})}}"></div>
                                    <div class="btn_main">
                                    <div class="buy_bt">
                                        <div class="mb-1">
                                            <form action="{{route('cartstore')}}" method="post">
                                                @csrf
                                                <input type="hidden" value="{{ $product->{'id'} }}" name="product_id">
                                                <button class="btn btn-secondary" type="submit"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Ajouter</button>
                                            </form>
                                         </div>
                                        {{--  <form action="{{route('addproducttocart')}}" method="post">
                                            @csrf
                                            <input type="hidden" value="{{ $product->{'id'} }}" name="product_id">
                                            <input type="hidden" value="{{ $product->{'price'} }}" name="price">
                                            <input type="hidden" value="1" name="quantity">

                                            <input class="btn btn-warning" type="submit" value="Acheter">
                                        </form>  --}}
                                    </div>
                                    <div class="seemore_bt"><a href="{{route('singleproduct', [$product->{'id'}, $product->{'slug'}])}}">Voir plus...</a></div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
