<?php
if (!function_exists('api_response')) {
    function api_response(int $code, string $message = null, $data = null): \Illuminate\Http\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        return response([
            'code' => $code,
            'message' => $message,
            'data' => $data
        ]);
    }
}

function getPrice($priceInDecimal)
{
    $price = floatval($priceInDecimal);
    return number_format($price, 2, ',', ' ') . ' XAF';
}

if (!function_exists('public_path')) {
    /**
     * Get the path to the public folder.
     *
     * @param  string  $path
     * @return string
     */
    function public_path($path = '')
    {
        return app()->make('path.public') . ($path ? DIRECTORY_SEPARATOR . ltrim($path, DIRECTORY_SEPARATOR) : $path);
    }
}
