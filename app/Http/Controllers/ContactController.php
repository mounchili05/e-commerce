<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function contactUs()
    {
        return view('user_template.contactUs');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
        ]);

        Contact::insert([
            'name' => $request->{'name'},
            'email' => $request->{'email'},
            'subject' => $request->{'subject'},
            'message' => $request->{'message'},
        ]);

        return redirect()->route('contactus')->with('message', 'Votre message a bien été envoyé!');
    }
}
