<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Inertia\Inertia;
use Inertia\Response;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): Response
    {
        return Inertia::render('Auth/Register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:' . User::class,
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }

    public function register(Request $request)
    {
        return view('auth.register2');
    }

    public function registerAdmin()
    {
        $roles = Role::all();
        return view('auth.register-admin', compact('roles'));
    }

    public function storeregister(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:' . User::class,
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'user_type' => "user",
            'password' => Hash::make($request->password),
        ]);

        $user->attachRole("user");

        event(new Registered($user));

        Auth::login($user);

        session()->flash('message', 'Votre compte a été créé avec succès!');

        return redirect()->route('home');
    }

    public function storeregisterAdmin(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:' . User::class,
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $roleName = Role::where('id', $request->{'role_id'})->value('name');
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'user_type' => $roleName,
            'password' => Hash::make($request->password),
        ]);

        $user->attachRole("admin");

        event(new Registered($user));

        Auth::login($user);

        session()->flash('message', 'Le compte a été créé avec succès!');

        return redirect()->route('pendingorder');
    }
}
