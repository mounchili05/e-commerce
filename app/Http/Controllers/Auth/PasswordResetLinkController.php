<?php

namespace App\Http\Controllers\Auth;

use App\Events\SendEmailResetPasswordEvent;
use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;
use Inertia\Response;

class PasswordResetLinkController extends Controller
{
    const RESET_EMAIL = "email";

    /**
     * Display the password reset link request view.
     */
    public function create(): Response
    {
        return Inertia::render('Auth/ForgotPassword', [
            'status' => session('status'),
        ]);
    }

    /**
     * Handle an incoming password reset link request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'email' => 'required|email',
        ]);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $status = Password::sendResetLink(
            $request->only('email')
        );

        if ($status == Password::RESET_LINK_SENT) {
            return back()->with('status', __($status));
        }

        throw ValidationException::withMessages([
            'email' => [trans($status)],
        ]);
    }

    public function passwordResetLink()
    {
        return view('auth.forgot-password2', [
            'status' => session('status')
        ]);
    }

    public function forgotPasswordStore(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
        ]);

        $user = User::where('email', $request->{'email'})->first();

        if (filled($user)) {
            $fullName = $user->{'name'};
            $resetPasswordToken = md5(uniqid()) . $request->{'email'} . sha1($request->{'email'});
            $passwordReset = PasswordReset::where('email', $request->{'email'})->first();
            if (!filled($passwordReset)) {
                PasswordReset::insert([
                    'email' => $request->{'email'},
                    'token' => $resetPasswordToken,
                    'created_at' => now()
                ]);
            }

            //SEND LINK RESET EMAIL
            event(new SendEmailResetPasswordEvent(
                $user,
                $resetPasswordToken
            ));

            $message = "Nous vous avons envoyé un mail de réinitialisation de votre mot de passe, veuillez vérifier votre boîte de reception";
            return back()->withErrors(['email-success' => $message])->with('old_email', $request->{'email'})
                ->with('success', $message);
        } else {
            $message = "L'adresse email que vous avez entrer n'existe pas!";
            return back()->withErrors(['email-error' => $message])->with('old_email', $request->{'email'})
                ->with('danger', $message);
        }
    }

    /**
     * Show forgot password form
     *
     * @param Request $request
     * @return View|Factory|Application
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function showForgotPasswordForm(Request $request)
    {
        $method = $request->get('m');

        if ($method === self::RESET_EMAIL) {
            return view('auth.reset-password-form');
        }
    }
}
