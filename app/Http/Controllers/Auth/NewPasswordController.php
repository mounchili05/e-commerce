<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;
use Inertia\Response;

use App\Concerns\VerifyEmailConcern;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class NewPasswordController extends Controller
{
    use VerifyEmailConcern;
    const RESET_EMAIL = "email";

    /**
     * Display the password reset view.
     */
    public function create(Request $request): Response
    {
        return Inertia::render('Auth/ResetPassword', [
            'email' => $request->email,
            'token' => $request->route('token'),
        ]);
    }

    /**
     * Handle an incoming new password request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($request->password),
                    'remember_token' => Str::random(60),
                ])->save();

                event(new PasswordReset($user));
            }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        if ($status == Password::PASSWORD_RESET) {
            return redirect()->route('login')->with('status', __($status));
        }

        throw ValidationException::withMessages([
            'email' => [trans($status)],
        ]);
    }

    public function resetPasswordForm(Request $request)
    {
        $token = $request->get('token');

        $resetToken = $this->verifyToken($token);

        if ($resetToken !== null) {

            return view('auth.reset-password-form', [
                'token' => $resetToken->{'token'},
                'email' => $resetToken->{'email'},
            ]);
        }

        return to_route('forgot-password', ['m' => self::RESET_EMAIL])->withErrors([
            'error' => __('errors.invalid_reset_password_link')
        ]);
    }

    /**
     * Reset the given user's password.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function reset(Request $request): RedirectResponse
    {
        $token = $request->get('token');
        $email = $request->get('email');

        DB::beginTransaction();

        try {
            if ($request->{'password'} != $request->{'confirm-password'}) {
                $message = "Les deux mots de passe ne correspondent pas";
                return back()->withErrors(['password-error' => $message])->with('danger', $message);
            }

            $user = User::where('email', $request->{'email'})->first();

            $user->update([
                'password' => Hash::make($request->get('password')),
            ]);

            // Delete token
            if (filled($token)) {
                $this->deleteResetToken($token);
            }

            // Revoke session
            if (filled($email)) {
                session()->forget('email');
            }

            DB::commit();

            if (filled($email)) {
                return to_route('login', ['q' => 'email'])->with([
                    'status' => "Mot de passe changer avec succès"
                ]);
            }

            return to_route('login')->with([
                'status' => "Mot de passe changer avec succès"
            ]);
        } catch (\Exception) {
            DB::rollBack();
        }

        return redirect()->back()->withErrors([
            'error' => "Nous ne pouvons pas changer votre mot de passe, une erreur est survenue"
        ]);
    }
}
