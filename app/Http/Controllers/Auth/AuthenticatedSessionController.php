<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Role;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use Inertia\Response;

use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Validation\ValidationException;


class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     */
    public function create(): Response
    {
        return Inertia::render('Auth/Login', [
            'canResetPassword' => Route::has('password.request'),
            'status' => session('status'),
        ]);
    }

    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        $request->authenticate();

        $request->session()->regenerate();

        return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    public function loginForm()
    {
        return view('auth.login2', [
            'canResetPassword' => Route::has('passwordrequest'),
            'status' => session('status'),
        ]);
    }

    public function loginFormAdmin()
    {
        return view('auth.login2-admin');
    }

    public function login(LoginRequest $request)
    {
        $message = $request->validate([
            'email' => 'required|string|email|max:255',
            'password' => ['required'],
        ]);

        $users = User::all();

        $email = $request->{'email'};
        $password = $request->{'password'};

        if (!empty($email) && !empty($password)) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                foreach ($users as $user) {
                    if ($user->{'email'} == $email && Hash::check($password, $user->{'password'})) {
                        $userName = $user->{'name'};

                        if ($user->{'user_type'} == 'super' || $user->{'user_type'} == 'admin') {

                            if (!Auth::attempt($request->only('email', 'password'), $request->boolean('remember'))) {
                                RateLimiter::hit($request->throttleKey());

                                throw ValidationException::withMessages([
                                    'email' => trans('auth.failed'),
                                ]);
                            }

                            RateLimiter::clear($request->throttleKey());

                            $request->session()->regenerate();
                            return redirect()->route('pendingorder');
                        } else {

                            if (!Auth::attempt($request->only('email', 'password'), $request->boolean('remember'))) {
                                RateLimiter::hit($request->throttleKey());

                                throw ValidationException::withMessages([
                                    'email' => trans('auth.failed'),
                                ]);
                            }

                            RateLimiter::clear($request->throttleKey());

                            $request->session()->regenerate();

                            return redirect()->intended(RouteServiceProvider::HOME, 302, ['name' => $userName]);
                        }
                    }

                    if (!$user->{'email'} == $email || !Hash::check($password, $user->{'password'})) {
                        session()->flash('danger', 'Login ou mot de passe incorrect!');
                        return redirect()->back();
                    }
                }
            } else {
                session()->flash('message', 'Veuillez saisir un email correct!');
                return redirect()->back();
            }
        } else {
            session()->flash('message', 'Veuillez remplir tous les champs!');
            return redirect()->back();
        }
    }
}
