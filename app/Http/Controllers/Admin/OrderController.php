<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\ShippingInfo;
use App\Models\Subcategory;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class OrderController extends Controller
{
    public function index()
    {
        $pendingOrders = ShippingInfo::whereNull('deleted_at')->latest()->get();
        $categories = Category::whereNull('deleted_at')->latest()->get();
        $subCategories = Subcategory::whereNull('deleted_at')->latest()->get();
        $products = Product::whereNull('deleted_at')->where('total_price_seller', '<>', 0)->latest()->get();

        return view('admin.pendingOrders', compact('pendingOrders', 'categories', 'subCategories', 'products'));
    }

    public function confirmOrder($id, $quantity, $productName, $price)
    {
        ShippingInfo::findOrFail($id)->update([
            'status' => "Livré",
            'deleted_at' => now(),
        ]);

        Product::where('product_name', $productName)->decrement('quantity', (int)$quantity);
        Product::where('product_name', $productName)->increment('quantity_seller_count', (int)$quantity);
        Product::where('product_name', $productName)->increment('total_price_seller', $price);

        return redirect()->route('pendingorder')->with('message', 'Livraison confirmée avec succès!');
    }

    public function listPersonsView()
    {
        return view('admin.printOrder');
    }

    public function listPersons()
    {
        $persons = ShippingInfo::query()->whereNotNull('name')->whereNull('deleted_at')->pluck('name', 'phone_number')->toArray();

        $result = [];
        $result = array_unique($persons);
        return api_response(100, null, $result);
    }

    public function printOrder(Request $request)
    {
        $orders = ShippingInfo::query()->where('phone_number', $request->{'personPhone'})->get();
        $order = ShippingInfo::query()->where('phone_number', $request->{'personPhone'})->first();
        $orderLocality = $order->{'city_name'};
        $orderCreated = $order->{'created_at'};
        $orderName = $order->{'name'};
        $orderPhone = $order->{'phone_number'};
        $orderId = $order->{'id'};

        $shippingCost = $order->{'shipping_cost'};

        /* if (Storage::directoryMissing('public/docs/orders')) {

            Storage::makeDirectory('public/docs/orders');
        } */

        $path = public_path() . '/files/commandes/';

        if (!file_exists($path)) {
            File::makeDirectory($path);
        }
        $nameFile = $order->id . '.' . 'pdf';
        $pdf = Pdf::loadView('admin.printOrderFile', compact('orders', 'orderLocality', 'orderCreated', 'orderName', 'orderPhone', 'orderId', 'shippingCost'))->setOptions(['defaultFont' => 'sans-serif', 'isHTML5ParserEnabled' => true,]);
        //->save($fileName = public_path() . '/files/commandes/commande' . $nameFile);
        $pdf->output();
        $dom_pdf = $pdf->getDomPDF();
        $pdf->save($fileName = public_path() . '/files/commandes/commande' . $nameFile);
        $url = asset('files/commandes/commande' . $nameFile);
        //dd($fileName);

        return api_response(100, null, $url);
    }
}
