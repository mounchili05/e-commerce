<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Subcategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    public function index()
    {
        $subCategories = Subcategory::latest()->get();
        return view('admin.allsubCategory', compact('subCategories'));
    }

    public function addSubCategory()
    {
        $categories = Category::latest()->get();
        return view('admin.addSubCategory', compact('categories'));
    }

    public function storeSubCategory(Request $request)
    {
        $request->validate([
            "subcategory_name" => 'required|unique:subcategories',
            "category_id" => 'required'
        ]);

        $category_name = Category::where('id', $request->{'category_id'})->value('category_name');
        Subcategory::insert([
            'subcategory_name' => $request->{'subcategory_name'},
            'category_id' => $request->{'category_id'},
            'category_name' => $category_name,
            'slug' => strtolower(str_replace(' ', '-', $request->{'subcategory_name'})),
        ]);

        Category::where('id', $request->{'category_id'})->increment('subcategory_count', 1);

        return redirect()->route('allsubcategory')->with('message', 'Sous-catégorie ajoutée avec succès!');
    }

    //Modifier sous catégorie
    public function editSubCategory($id)
    {
        $subCategoryInfo = SubCategory::findOrfail($id);
        $categories = Category::latest()->get();
        return view('admin.editSubCategory', compact('subCategoryInfo', 'categories'));
    }

    public function updateSubCategory(Request $request)
    {
        $id = $request->{'id'};
        $request->validate([
            "subcategory_name" => 'required|unique:subcategories',
            //"category_id" => 'required'
        ]);

        $result = SubCategory::findOrFail($id)->updateService([
            'subcategory_name' => $request->{'subcategory_name'},
            //'category_id' => $request->{'category_id'},
            'slug' => strtolower(str_replace(' ', '-', $request->{'subcategory_name'})),
        ]);

        $product = Product::query()->where('category_id', $id)->first();
        if (filled($product)) {
            $product->update([
                'product_category_name' => $result->{'product_subcategory_name'}
            ]);
        }

        return redirect()->route('allsubcategory')->with('message', 'Sous-catégorie mise à jour avec succès!');
    }

    public function deleteSubCategory($id)
    {
        $catId = Subcategory::where('id', $id)->value('category_id');
        SubCategory::findOrFail($id)->delete();
        Category::where('id', $catId)->decrement('subcategory_count', 1);

        $products = Product::where('subcategory_id', $id)->get();
        foreach ($products as $product) {
            $product->delete();
        }

        return redirect()->route('allsubcategory')->with('message', 'Sous-catégorie supprimée avec succès');
    }
}
