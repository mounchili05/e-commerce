<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Subcategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::latest()->get();
        return view('admin.allCategory', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin.addCategory');
    }

    public function storeCategory(Request $request)
    {
        $request->validate([
            "category_name" => 'required|unique:categories'
        ]);

        Category::insert([
            'category_name' => $request->{'category_name'},
            'slug' => strtolower(str_replace(' ', '-', $request->{'category_name'})),
        ]);

        return redirect()->route('allcategory')->with('message', 'Catégorie ajoutée avec succès!');
    }

    public function editCategory($id)
    {
        $category_info = Category::findOrfail($id);
        return view('admin.editCategory', compact('category_info'));
    }

    public function updateCategory(Request $request)
    {
        $id = $request->{'id'};
        $request->validate([
            "category_name" => 'required|unique:categories'
        ]);

        $category = Category::findOrFail($id)->updateService([
            'category_name' => $request->{'category_name'},
            'slug' => strtolower(str_replace(' ', '-', $request->{'category_name'})),
        ]);

        $product = Product::query()->where('category_id', $id)->first();
        if (filled($product)) {
            $product->update([
                'product_category_name' => $category->{'category_name'}
            ]);
        }

        return redirect()->route('allcategory')->with('message', 'Catégorie mise à jour avec succès!');
    }

    public function deleteCategory($id)
    {
        Category::findOrFail($id)->delete();

        $subCats = Subcategory::where('category_id', $id)->get();
        foreach ($subCats as $subCat) {
            $subCat->delete();
        }

        $products = Product::where('category_id', $id)->get();
        foreach ($products as $product) {
            $product->delete();
        }

        return redirect()->route('allcategory')->with('message', 'Catégorie supprimée avec succès');
    }
}
