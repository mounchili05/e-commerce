<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Town;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class TownController extends Controller
{
    public function index()
    {
        $towns = Town::latest()->get();
        return view('admin.allTown', compact('towns'));
    }

    public function addTown()
    {
        return view('admin.addTown');
    }

    public function storeTown(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "town_name" => 'required',
            "district" => 'required|unique:towns',
        ]);

        if ($validator->fails()) {
            Session::flash("danger", "Le nom et le quartie la ville sont obligatoires, veuillez tous les remplir ! ");
            return back()->with('message', "Erreur de validation des données !");
        }

        Town::insert([
            'town_name' => $request->{'town_name'},
            'code' => $request->{'code'},
            'shipping_cost' => $request->{'shipping_cost'},
            'country_name' => $request->{'country_name'},
            'district' => $request->{'district'},
        ]);

        return redirect()->route('alltown')->with('message', 'Ville ajoutée avec succès!');
    }

    public function editTown($id)
    {
        $town_info = Town::findOrfail($id);
        return view('admin.edittown', compact('town_info'));
    }

    public function updateTown(Request $request)
    {
        $id = $request->{'id'};
        $validator = Validator::make($request->all(), [
            "town_name" => 'required',
            "district" => 'required|unique:towns',
        ]);

        if ($validator->fails()) {
            Session::flash("danger", "Le nom et le quartie la ville sont obligatoires, veuillez tous les remplir ! ");
            return back()->with('message', "Erreur de validation des données !");
        }

        $town = Town::findOrFail($id)->updateService([
            'town_name' => $request->{'town_name'},
            'code' => $request->{'code'},
            'shipping_cost' => $request->{'shipping_cost'},
            'country_name' => $request->{'country_name'},
            'district' => $request->{'district'},
        ]);

        return redirect()->route('alltown')->with('message', 'Ville mise à jour avec succès!');
    }

    public function deleteTown($id)
    {
        Town::findOrFail($id)->delete();
        return redirect()->route('alltown')->with('message', 'ville supprimée avec succès');
    }
}
