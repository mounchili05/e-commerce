<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Subcategory;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::latest()->get();
        return view('admin.allProducts', compact('products'));
    }

    public function addProduct()
    {
        $categories = Category::latest()->get();
        $subCategories = Subcategory::latest()->get();
        return view('admin.addProduct', compact('categories', 'subCategories'));
    }

    public function storeProduct(Request $request)
    {
        $requestValidate = $request->validate([
            'product_name' => 'required|unique:products',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'price' => 'required',
            'quantity' => 'required',
            'product_short_desc' => 'required',
            'product_desc' => 'required',
            'product_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $image = $request->file('product_img');
        $img_name = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
        $request->{'product_img'}->move(public_path('upload'), $img_name);
        $img_url = 'upload/' . $img_name;

        $category_name = Category::where('id', $request->{'category_id'})->value('category_name');
        $subCategory_name = Subcategory::where('id', $request->{'subcategory_id'})->value('subcategory_name');

        Product::insert([
            'product_name' => $request->{'product_name'},
            'code' => $request->{'code'},
            'product_short_desc' => $request->{'product_short_desc'},
            'product_desc' => $request->{'product_desc'},
            'price' => $request->{'price'},
            'product_category_name' => $category_name,
            'product_subcategory_name' => $subCategory_name,
            'category_id' => $request->{'category_id'},
            'subcategory_id' => $request->{'subcategory_id'},
            'product_img' => $img_url,
            'quantity' => $request->{'quantity'},
            'slug' => strtolower(str_replace(' ', '-', $request->{'product_name'})),
        ]);

        Category::where('id', $request->{'category_id'})->increment('product_count', 1);
        SubCategory::where('id', $request->{'subcategory_id'})->increment('product_count', 1);

        return redirect()->route('allproducts')->with('message', 'Article ajouté avec succès!');
    }

    public function editProductImg($id)
    {
        $productInfo = Product::findOrFail($id);
        return view('admin.editProductImg', compact('productInfo'));
    }

    public function updateProductImg(Request $request)
    {
        $request->validate([
            'product_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $id = $request->{'id'};
        $image = $request->file('product_img');
        $img_name = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
        $request->{'product_img'}->move(public_path('upload'), $img_name);
        $img_url = 'upload/' . $img_name;

        $category_name = Category::where('id', $request->{'category_id'})->value('category_name');
        $subCategory_name = Subcategory::where('id', $request->{'subcategory_id'})->value('subcategory_name');

        Product::findOrFail($id)->update([
            'product_img' => $img_url,
        ]);

        return redirect()->route('allproducts')->with('message', 'Image modifiée avec succès!');
    }

    public function editProduct($id)
    {
        $productInfo = Product::findOrFail($id);
        return view('admin.editProduct', compact('productInfo'));
    }

    public function updateProduct(Request $request)
    {
        $productId = $request->{'id'};

        $request->validate([
            'product_name' => 'required',
            'price' => 'required',
            'quantity' => 'required',
            'product_short_desc' => 'required',
            'product_desc' => 'required',
        ]);

        Product::findOrFail($productId)->update([
            'product_name' => $request->{'product_name'},
            'code' => $request->{'code'},
            'product_short_desc' => $request->{'product_short_desc'},
            'product_desc' => $request->{'product_desc'},
            'price' => $request->{'price'},
            'quantity' => $request->{'quantity'},
            'slug' => strtolower(str_replace(' ', '-', $request->{'product_name'})),
        ]);

        return redirect()->route('allproducts')->with('message', 'Article modifié avec succès!');
    }

    public function deleteProduct($id)
    {
        $catId = Product::where('id', $id)->value('category_id');
        $subCatId = Product::where('id', $id)->value('subcategory_id');

        Product::findOrFail($id)->delete();

        Category::where('id', $catId)->decrement('product_count', 1);
        SubCategory::where('id', $subCatId)->decrement('product_count', 1);

        return redirect()->route('allproducts')->with('message', 'Article supprimé avec succès!');
    }

    public function listUser()
    {
        $users = User::query()->where('user_type', '<>', "user")->get();
        $roles = Role::latest()->get();
        return view('admin.adminAccount', compact('users', 'roles'));
    }

    public function createUser(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:' . User::class,
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        try {
            $roleName = Role::where('id', $request->{'role_id'})->value('name');

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone1' => $request->{'phone1'},
                'phone2' => $request->{'phone2'},
                'id_card_number' => $request->{'id_card_number'},
                'place_of_residence' => $request->{'place_of_residence'},
                'date_of_birth' => $request->{'date_of_birth'},
                'user_type' => $roleName,
                'password' => Hash::make($request->password),
            ]);

            if ($roleName == "admin") {
                $user->attachRole("admin");
            }
            if ($roleName == "super") {
                $user->attachRole("super");
            }
            if ($roleName == "user") {
                $user->attachRole("user");
            }

            event(new Registered($user));

            //Auth::login($user);

            session()->flash('message', 'Le compte a été créé avec succès!');

            return api_response(100);
        } catch (\Exception $ex) {
            return api_response(103, $ex->getMessage());
        }
    }

    public function editUser(Request $request)
    {
        $request->validate([
            "id" => 'required'
        ]);

        try {
            $id = $request->{"id"};

            $user = User::findOrFail($id);

            $roleName = Role::where('id', $request->{'role_id'})->value('name');
            $user->update([
                'name' => $request->name,
                'email' => $request->email,
                'phone1' => $request->{'phone1'},
                'phone2' => $request->{'phone2'},
                'id_card_number' => $request->{'id_card_number'},
                'place_of_residence' => $request->{'place_of_residence'},
                'date_of_birth' => $request->{'date_of_birth'},
                'user_type' => $roleName,
            ]);

            $roleUser = RoleUser::query()->where('user_id', $user->{'id'})->first();

            /* if ($roleName == "admin") {
                $roleUser->update([
                    'role_id' => $request->{'role_id'},
                ]);
                DB::statement("UPDATE role_user SET role_id = '%$request->{'role_id'}%' WHERE user_id = '% $user->{'id'}%' ' ");
                //$user->attachRole("admin");
            }
            if ($roleName == "super") {
                $roleUser->update([
                    'role_id' => $request->{'role_id'},
                ]);
                //$user->attachRole("super");
            }
            if ($roleName == "user") {
                $roleUser->update([
                    'role_id' => $request->{'role_id'},
                ]);
                //$user->attachRole("user");
            } */

            event(new Registered($user));

            //Auth::login($user);

            session()->flash('message', 'Le compte a été modifié avec succès!');

            return api_response(100);
        } catch (\Exception $ex) {
            return api_response(103, $ex->getMessage());
        }
    }

    public function deleteUser(Request $request)
    {
        $request->validate([
            "id" => 'required'
        ]);

        try {
            $id = $request->{"id"};
            $user = User::findOrFail($id);

            $user->delete();

            return api_response(100);
        } catch (\Exception $ex) {
            return api_response(103, $ex->getMessage());
        }
    }

    public function search()
    {
        request()->validate([
            'q' => 'required|min:1'
        ]);
        $q = request()->input('q');

        $products = Product::where('product_name', 'like', "%$q%")
            ->orWhere('product_short_desc', 'like', "%$q%")
            ->paginate(10);

        return view('user_template.search')->with('products', $products);
    }
}
