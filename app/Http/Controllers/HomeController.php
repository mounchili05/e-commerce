<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $allProducts = Product::latest()->paginate(10);
        return view('user_template.home', compact('allProducts'));
    }
}
