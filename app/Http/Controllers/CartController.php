<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ShippingInfo;
use App\Models\Town;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user_template.cart.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $duplicata = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id === (int)$request->product_id;
        });

        if ($duplicata->isNotEmpty()) {
            return redirect()->route('home')->with('message', "L'article a déjà été ajouté au panier!");
        }

        $product = Product::findOrFail($request->product_id);

        Cart::add($product->id, $product->product_name, 1, $product->price)
            ->associate('App\Product');

        return redirect()->route('home')->with('message', 'Votre article a été ajouté au panier avec succès!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $rowId)
    {
        //$data = $request->json->all();
        $validator = Validator::make($request->all(), [
            'qty' => "required|numeric|between:1,$request->qtyMax"
        ]);

        if ($validator->fails()) {
            Session::flash("danger", "La quantité de l'article ne doit pas dépasser " . $request->qtyMax . '.');
            return response()->json(['error' => 'La quantité dans le panier ne peut pas être mise à jour']);
        }

        $cartUpdate = Cart::update($rowId, $request->qty);

        Session::flash("message", "La quantité de l'article est passée à " . $request->qty . '.');

        return response()->json(['message' => 'La quantité dans le panier a été mise à jour']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($rowid)
    {
        Cart::remove($rowid);

        return back()->with('message', "L'article a été rétiré du panier !");
    }

    public function addShippingAddress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => "required",
            'phone_number' => "required",
            'city_name' => "required",
            'district' => "required",
        ]);

        if ($validator->fails()) {
            Session::flash("danger", "Le nom, le téléphone et la ville ou le quartier sont obligatoires, veuillez tous les remplir ! ");
            return back()->with('message', "Erreur de validation des données !");
        }

        foreach (Cart::content() as $cartItem) {

            $productName = Product::where('id', $cartItem->id)->value('product_name');
            $town = Town::where('district', $request->district)->first();
            $shippingCost = $town->{'shipping_cost'};

            ShippingInfo::insert([
                'name' => $request->{'name'},
                'phone_number' => $request->{'phone_number'},
                'quantity' => $cartItem->qty,
                'product_name' => $productName,
                'price' => $cartItem->subtotal(),
                'city_name' => $request->{'city_name'},
                'postal_code' => $request->{'postal_code'},
                'shipping_cost' => $shippingCost,
            ]);
        }

        Cart::destroy();

        Session::flash("message", "Votre commande à bien été enregistrée, merci de nous faire confiance ! ");

        return redirect()->route('home');
    }

    public function getShippingCost(Request $request)
    {
        $town = Town::query()->where('district', $request->{'district'})->whereNull('deleted_at')->first();
        $shippingCost = getPrice($town->{'shipping_cost'});

        return api_response(100, null, $shippingCost);
    }
}
