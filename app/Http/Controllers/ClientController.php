<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\Role;
use App\Models\ShippingInfo;
use App\Models\Subcategory;
use App\Models\Town;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Auth\Events\Registered;

class ClientController extends Controller
{
    public function categoryPage($id)
    {
        $category = Category::findOrFail($id);
        $products = Product::where('category_id', $id)->latest()->get();
        return view('user_template.category', compact('category', 'products'));
    }

    public function subCategoryPage($id)
    {
        $subCategory = Subcategory::findOrFail($id);
        $products = Product::where('subcategory_id', $id)->latest()->get();
        return view('user_template.subCategory', compact('subCategory', 'products'));
    }

    public function singleProduct($id)
    {
        $product = Product::findOrFail($id);
        $subCatId = Product::where('id', $id)->value('subcategory_id');
        $relatedProducts = Product::where('subcategory_id', $subCatId)->latest()->get();
        return view('user_template.product', compact('product', 'relatedProducts'));
    }

    public function addToCart()
    {
        $userId = Auth::id();
        $cartItems = Cart::where('user_id', $userId)->get();
        return view('user_template.addToCart', compact('cartItems'));
    }

    public function addProductToCart(Request $request)
    {
        $productPrice = $request->{'price'};
        $productQuantity = $request->{'quantity'};
        $price = $productPrice * $productQuantity;

        Cart::insert([
            'product_id' => $request->{'product_id'},
            'price' => $price,
            'quantity' => $request->{'quantity'},
            'user_id' => Auth::id(),
            'quantity' => $request->{'quantity'},
        ]);

        return redirect()->route('addtocart')->with('message', 'Votre article a été ajouté au panier avec succès!');
    }

    public function removeCartItem($id)
    {
        Cart::findOrFail($id)->delete();
        return redirect()->route('addtocart')->with('message', 'L\'article a été retiré au panier avec succès!');
    }

    public function GetShippingAdrdress()
    {
        $towns = Town::latest()->get();
        return view('user_template.shippingAddress', compact('towns'));
    }


    public function checkout()
    {
        $userId = Auth::id();
        $cartItems = Cart::where('user_id', $userId)->get();
        $shippingAddress = ShippingInfo::where('user_id', $userId)->first();
        return view('user_template.checkout', compact('cartItems', 'shippingAddress'));
    }

    public function placeOrder()
    {
        $userId = Auth::id();
        $shippingAddress = ShippingInfo::where('user_id', $userId)->first();
        $cartItems = Cart::where('user_id', $userId)->get();

        foreach ($cartItems as $item) {
            Order::insert([
                'user_id' => $userId,
                'shipping_phone_number' => $shippingAddress->{'phone_number'},
                'shipping_city' => $shippingAddress->{'city_name'},
                'shipping_postal_code' => $shippingAddress->{'postal_code'},
                'product_name' => $item->{'product'}->{'product_name'},
                'quantity' => $item->{'quantity'},
                'total_price' => $item->{'price'},
            ]);

            Cart::findOrFail($item->{'id'})->delete();
        }

        ShippingInfo::where('user_id', $userId)->first()->delete();

        return redirect()->route('pendingorders')->with('message', 'Votre commande a été passée avec succès');
    }

    public function userProfile()
    {
        return view('user_template.userProfile');
    }

    public function pendingOrders()
    {
        $pendingOrders = Order::where('status', 'En attente')->latest()->get();
        return view('user_template.pendingOrders', compact('pendingOrders'));
    }

    public function history()
    {
        return view('user_template.history');
    }

    public function newRelease()
    {
        return view('user_template.newRelease');
    }

    public function todaysDeal()
    {
        return view('user_template.todaysDeal');
    }

    public function customerService()
    {
        return view('user_template.customerService');
    }
}
