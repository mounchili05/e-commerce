<?php

namespace App\Listeners;

use App\Events\SendEmailResetPasswordEvent;
use App\Exceptions\SendEmailResetPasswordException;
use App\Mail\ResetPasswordMail;
use Illuminate\Mail\Mailer;

class SendEmailResetPasswordListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(private Mailer $mailer)
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SendConfirmationOfRequestRegistrationEvent $event
     * @return void
     */
    public function handle(SendEmailResetPasswordEvent $event): void
    {
        $email = $event->user->{'email'};

        if (filled($email)) {
            try {
                $this->mailer->send(new ResetPasswordMail(
                    $event->user,
                ));
            } catch (\Exception $exception) {
                throw (new SendEmailResetPasswordException($exception->getMessage()));
            }
        }
    }
}
