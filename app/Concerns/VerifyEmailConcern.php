<?php

namespace App\Concerns;

use Illuminate\Support\Facades\DB;

trait VerifyEmailConcern
{
    /**
     * Verify reset token
     *
     * @param string|null $token
     * @return mixed
     */

    public function verifyToken(?string $token): mixed
    {
        if (filled($token)) {
            $resetToken = DB::table('password_resets')->where('token', $token)->first();
            $diffInMinutes = now()->diffInMinutes($resetToken->{'created_at'});

            if ($diffInMinutes > $this->tokenValidityInMinutes()) {
                $this->deleteResetToken($resetToken->{'token'});

                return null;
            }

            return $resetToken;
        }

        return null;
    }

    /**
     * Delete token
     *
     * @param string $token
     * @return void
     */
    public function deleteResetToken(string $token): void
    {
        DB::table('password_resets')->where('token', $token)->delete();
    }

    /**
     * Validity in minutes
     *
     * @return int
     */
    public function tokenValidityInMinutes(): int
    {
        return 30;
    }
}
