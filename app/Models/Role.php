<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    use HasFactory;
    protected $fillable = [
        'name',
        'display_name',
        'description',
    ];
    public $guarded = [];
}
