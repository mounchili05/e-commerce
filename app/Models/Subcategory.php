<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    use HasFactory;
    protected $fillable = [
        'subcategory_name',
        'slug',
        'category_name',
        'category_id',
        'product_count',
    ];

    //Relations
    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    //Functions
    public function updateService(array $data): Model|Builder
    {
        return tap($this)->update($data);
    }
}
