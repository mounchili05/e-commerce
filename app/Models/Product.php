<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'product_name',
        'price',
        'product_category_name',
        'product_subcategory_name',
        'slug',
        'product_short_desc',
        'product_desc',
        'category_id',
        'subcategory_id',
        'product_img',
        'quantity',
        'code',
        'total_price_seller',
        'quantity_seller_count',
    ];

    //Relations
    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
    public function subcategory(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(subcategory::class);
    }

    //Functions
    public function updateService(array $data): Model|Builder
    {
        return tap($this)->update($data);
    }
}
