<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_name',
        'slug',
        'subcategory_count',
        'product_count',
    ];

    //Functions
    public function updateService(array $data): Model|Builder
    {
        return tap($this)->update($data);
    }
}
