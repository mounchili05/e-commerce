<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    use HasFactory;
    protected $fillable = [
        'town_name',
        'code',
        'shipping_cost',
        'country_name',
        'district',
    ];

    //Functions
    public function updateService(array $data): Model|Builder
    {
        return tap($this)->update($data);
    }
}
