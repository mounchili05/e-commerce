<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use Illuminate\Support\Facades\Route;

Route::middleware('guest')->group(function () {
    Route::get('register', [RegisteredUserController::class, 'create'])
        ->name('register');

    Route::post('register', [RegisteredUserController::class, 'store']);

    /* Route::get('login', [AuthenticatedSessionController::class, 'create'])
        ->name('login');

    Route::post('login', [AuthenticatedSessionController::class, 'store']);
 */
    Route::get('forgot-password', [PasswordResetLinkController::class, 'create'])
        ->name('password.request');

    Route::post('forgot-password', [PasswordResetLinkController::class, 'store'])
        ->name('password.email');

    /* Route::get('reset-password/{token}', [NewPasswordController::class, 'create'])
        ->name('password.reset');

    Route::post('reset-password', [NewPasswordController::class, 'store'])
        ->name('password.store'); */

    Route::get('/login', [AuthenticatedSessionController::class, 'loginForm'])->name('login');
    Route::get('/login-form-admin', [AuthenticatedSessionController::class, 'loginFormAdmin'])->name('loginformadmin');
    Route::get('/register-form', [RegisteredUserController::class, 'register'])->name('register2');
    Route::get('/register-form-admin', [RegisteredUserController::class, 'registerAdmin'])->name('registeradmin');
    Route::get('forgot-password2', [PasswordResetLinkController::class, 'passwordResetLink'])->name('passwordrequest');
    Route::post('/login2', [AuthenticatedSessionController::class, 'login'])->name('login2');


    Route::post('/register-store', [RegisteredUserController::class, 'storeRegister'])->name('registerstore');
    Route::post('/register-store-admin', [RegisteredUserController::class, 'storeRegisterAdmin'])->name('registerstoreadmin');
    Route::post('forgot-password2', [PasswordResetLinkController::class, 'forgotPasswordStore'])->name('passwordemailstore');
    Route::get('reset-password', [NewPasswordController::class, "resetPasswordForm"])->name('reset-password');
    Route::post('reset-password', [NewPasswordController::class, "reset"])->name('reset-password');
    Route::get('forgot-password', [PasswordResetLinkController::class, "showForgotPasswordForm"])->name('forgot-password');
});

Route::middleware('auth')->group(function () {
    Route::get('verify-email', EmailVerificationPromptController::class)
        ->name('verification.notice');

    Route::get('verify-email/{id}/{hash}', VerifyEmailController::class)
        ->middleware(['signed', 'throttle:6,1'])
        ->name('verification.verify');

    Route::post('email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
        ->middleware('throttle:6,1')
        ->name('verification.send');

    Route::get('confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->name('password.confirm');

    Route::post('confirm-password', [ConfirmablePasswordController::class, 'store']);

    Route::put('password', [PasswordController::class, 'update'])->name('password.update');

    Route::get('logout', [AuthenticatedSessionController::class, 'destroy'])
        ->name('logout');
});
