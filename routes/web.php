<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\SubCategoryController;
use App\Http\Controllers\Admin\TownController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

require __DIR__ . '/auth.php';

Route::controller(HomeController::class)->group(function () {
    Route::get('/', 'index')->name("home");
});

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::controller(ContactController::class)->group(function () {
    Route::get('/create', 'contactUs')->name('contactus');
    Route::post('/store', 'store')->name('contactusstore');
});

/** Cart routes */
Route::controller(CartController::class)->group(function () {
    Route::post('/cart/add/', 'store')->name("cartstore");
    Route::get('/cart/destroy/', 'destroy')->name("cartdestroy");
    Route::get('/cart', 'index')->name("cartindex");
    Route::post('/cart-update/{rowId}', 'update')->name("cartupdate");
    Route::delete('/cart/{rowid}', 'destroy')->name("cartremove");
    Route::post('/add-shipping-address', 'addShippingAddress')->name("addshippingaddress");
    Route::get('/shipping-cost', 'getShippingCost')->name("getshippingcost");
});

/** Checkout routes */
Route::controller(CheckoutController::class)->group(function () {
    Route::get('/paiement', 'index')->name("checkoutindex");
});

Route::get('/cart-destroy', function () {
    Cart::destroy();
})->name('cart.destroy');

Route::controller(ClientController::class)->group(function () {
    Route::get('/category/{id}/{slug}', 'categoryPage')->name("category");
    Route::get('/sub-category/{id}/{slug}', 'subCategoryPage')->name("subcategory");
    Route::get('/product-details/{id}/{slug}', 'singleProduct')->name("singleproduct");
    Route::get('/new-release', 'newRelease')->name("newrelease");
    Route::get('/user-profile/history', 'history')->name("history");
    Route::get('/new-release', 'newRelease')->name("newrelease");
    Route::get('/todays-deal', 'todaysDeal')->name("todaysdeal");
    Route::get('/customer-service', 'customerService')->name("customerservice");
    Route::get('/add-to-cart', 'addToCart')->name("addtocart");
    Route::post('/add-product-to-cart', 'addProductToCart')->name("addproducttocart");
    Route::get('/shipping-address', 'GetShippingAdrdress')->name("shippingaddress");
    Route::post('/place-order', 'placeOrder')->name("placeorder");
    Route::get('/checkout', 'checkout')->name("checkout");
});

Route::middleware(['auth', 'role:user'])->group(function () {
    Route::controller(ClientController::class)->group(function () {

        Route::get('/user-profile', 'userProfile')->name("userprofile");
        Route::get('/user-profile/pending-orders', 'pendingOrders')->name("pendingorders");
        Route::get('/remove-cart-item/{id}', 'removeCartItem')->name("removecartitem");
    });
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'role:user'])->name('dashboard');

Route::middleware(['auth', 'role:admin|super'])->group(function () {
    Route::controller(DashboardController::class)->group(function () {
        Route::get('/admin/dashboard', 'index')->name('admindashboard');
    });
    Route::controller(CategoryController::class)->group(function () {
        Route::get('/admin/all-category', 'index')->name('allcategory');
        Route::get('/admin/add-category', 'addCategory')->name('addcategory');
        Route::post('/store-category', 'storeCategory')->name('storecategory');
        Route::get('/edit-category/{id}', 'editCategory')->name('editcategory');
        Route::post('/update-category', 'updateCategory')->name('updatecategory');
        Route::get('/delete-category/{id}', 'deleteCategory')->name('deletecategory');
    });
    Route::controller(SubCategoryController::class)->group(function () {
        Route::get('/admin/all-subcategory', 'index')->name('allsubcategory');
        Route::get('/admin/add-subcategory', 'addSubCategory')->name('addsubcategory');
        Route::post('/store-subcategory', 'storeSubCategory')->name('storesubcategory');
        Route::get('/edit-subcategory/{id}', 'editSubCategory')->name('editsubcategory');
        Route::post('/update-subcategory', 'updateSubCategory')->name('updatesubcategory');
        Route::get('/delete-subcategory/{id}', 'deleteSubCategory')->name('deletesubcategory');
    });
    Route::controller(ProductController::class)->group(function () {
        Route::get('/admin/all-products', 'index')->name('allproducts');
        Route::get('/admin/add-product', 'addproduct')->name('addproduct');
        Route::post('/admin/store-product', 'storeProduct')->name('storeproduct');
        Route::get('/admin/edit-product-img/{id}', 'editProductImg')->name('editproductimg');
        Route::post('/update-product-img', 'updateProductImg')->name('updateproductimg');
        Route::get('/admin/edit-product/{id}', 'editProduct')->name('editproduct');
        Route::post('/update-product', 'updateProduct')->name('updateproduct');
        Route::get('/delete-product/{id}', 'deleteProduct')->name('deleteproduct');
    });

    Route::controller(OrderController::class)->group(function () {
        Route::get('/admin/list-person-view', 'listPersonsView')->name('listpersonsview');
        Route::get('/admin/list-person', 'listPersons')->name('listpersons');
    });

    Route::controller(TownController::class)->group(function () {
        Route::get('/admin/all-town', 'index')->name('alltown');
        Route::get('/admin/add-town', 'addTown')->name('addtown');
        Route::post('/store-town', 'storeTown')->name('storetown');
        Route::get('/edit-town/{id}', 'editTown')->name('edittown');
        Route::post('/update-town', 'updateTown')->name('updatetown');
        Route::get('/delete-town/{id}', 'deleteTown')->name('deletetown');
    });
});

Route::controller(ProductController::class)->group(function () {
    Route::get('/register-form-admin-list', 'listUser')->name('listuser');
    Route::post('/register-form-admin-edition', 'editUser')->name('useredition');
    Route::post('/register-form-admin-deletion', 'deleteUser')->name('userdeletion');
    Route::post('/register-form-admin-creation', 'createUser')->name('usercreation');
    Route::get('/search', 'search')->name('productssearch');
});

Route::controller(OrderController::class)->group(function () {
    Route::get('/admin/pending-order', 'index')->name('pendingorder');
    Route::get('/confirm-order/{id}/{quantity}/{productName}/{price}', 'confirmOrder')->name('confirmorder');
    Route::get('/print-order-file', 'printOrder')->name('printorderfile');
});
